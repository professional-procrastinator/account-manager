//tests for auth ig
import supertest from 'supertest';
import app from '../src/app';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import User from '../src/models/user';

dotenv.config();

beforeAll(async () => {
  await mongoose.connect(process.env.MONGODB_TEST_URI!);
  await User.deleteMany({});
});

test('Initialize', async () => {
  const users = await User.find({});

  expect(users).toHaveLength(0);
});

test('GET /auth/me - without logging in', async () => {
  //try to get the user session without logging in
  //expects success? false

  const response = await supertest(app).get('/auth/me');

  expect(response.body.success).toBe(false);
});

test('POST /auth/register - empty request', async () => {
  //send a request to /auth/register without any data
  //expects success? false
  const response = await supertest(app).post('/auth/register').send({
    name: '',
    email: '',
    password: '',
  });

  expect(response.body.success).toBe(false);
});

test('POST /auth/regiser - missing field (1/3)', async () => {
  //send a request to /auth/register without a name
  //expects success? false

  const response = await supertest(app).post('/auth/register').send({
    name: '',
    email: 'helloworld@example.com',
    password: 'h3ll0w0rld',
  });

  expect(response.body.success).toBe(false);
});

test('POST /auth/regiser - missing field (2/3)', async () => {
  //send a request to /auth/register without an email
  //expects success? false

  const response = await supertest(app).post('/auth/register').send({
    name: 'hello',
    email: '',
    password: 'h3ll0w0rld',
  });

  expect(response.body.success).toBe(false);
});

test('POST /auth/regiser - missing field (3/3)', async () => {
  //send a request to /auth/register without a password
  //expects success? false

  const response = await supertest(app).post('/auth/register').send({
    name: 'hello',
    email: 'helloworld@example.com',
    password: '',
  });

  expect(response.body.success).toBe(false);
});

afterAll(async () => {
  await User.deleteMany({});
  await mongoose.connection.close();
});
