export interface UserInterface {
  name: string;
  email: string;
  password: string;
  photoUrl: string;
  identities: [IdentityInterface?] | [string?];
  accounts: [AccountInterface?] | [string?];
  masterPassword?: string;
  selectedId?: string;
  _mpHash?: string;
}

export interface IdentityInterface {
  email: string;
  domain: string;
  websitePassword?: string;
  website: string;
  websitePhotoUrl: string;
  user: UserInterface | string;
}

export interface AccountInterface {
  email: string;
  websitePassword?: string;
  website: string;
  websitePhotoUrl: string;
  user: UserInterface | string;
}
