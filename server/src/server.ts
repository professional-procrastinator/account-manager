import app from './app';
import config from './config/index';
import mongoose from 'mongoose';

mongoose
  .connect(process.env.MONGODB_URI!)
  .then(() => console.log('Connected to MongoDB.......'))
  .catch((err) => console.log(err));

app.listen(config.PORT, () => {
  console.log(`Server is listening on port ${config.PORT}`);
});
