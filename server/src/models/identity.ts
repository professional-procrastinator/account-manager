import mongoose from "mongoose";
import { IdentityInterface } from "../@types/types";

export interface IdentityModel extends mongoose.Document, IdentityInterface {}

const identitySchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Email is Missing"],
    unique: true,

  },
  domain: {
    type: String,
    default: "altemail.co"
  },
  websitePassword: {
    type: String,
  },
  website: {
    type: String,
    required: [true, "Website is missing"],
  },
  websitePhotoUrl: {
    type: String,
    required: [true, "Website Picture is missing"],
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User is missing"],
  },
});

const Identity = mongoose.model("Identity", identitySchema);
export default Identity;
