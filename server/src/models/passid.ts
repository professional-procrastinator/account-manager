import mongoose from "mongoose";
import { IdentityInterface } from "../@types/types";

export interface PassIdModel extends mongoose.Document, IdentityInterface {}

const passIdSchema = new mongoose.Schema({
  uid: {
    type: String,
    required: [true, "UID is missing"],
    unique: true
  },
  user: {
    type: mongoose.Types.ObjectId,
    required: [true, "User is missing"],
  },
  userEmail: {
    type: String,
    required: true,
    unique: true,
  },
  createdAt: { type: Date, expires: "30m", default: Date.now },
  expireAt: {
    type: Date,
    default: Date.now,
    expires: "30m",
  },
});

const PassId = mongoose.model("passid", passIdSchema);
export default PassId;
