import mongoose from "mongoose";
import { UserInterface as UInterface } from "../@types/types";

export interface UserModel extends mongoose.Document, UInterface {}
export interface UserInterface extends UInterface {}

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Name is missing!"],
  },
  email: {
    type: String,
    unique: true,
    required: [true, "Email is missing"],
  },
  password: {
    type: String,
    required: [true, "Password is missing"],
  },
  photoUrl: {
    type: String,
    required: [true, "Photo URL is Missing"],
  },
  identities: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "Identity",
    default: [],
  },
  accounts: {
    type: [mongoose.Schema.Types.ObjectId],
    ref: "Account",
    default: [],
  },
  masterPassword: {
    type: String,
  },
  selectedId: {
    type: String,
  },
  _mpHash: {
    type: String
  },
  verified: {
    default: false,
    type: Boolean,
  },
  expireAt: {
    type: Date,
    default: Date.now,
    expires: "2hr",
  },
});

const UnverifiedUser = mongoose.model("UnverifiedUser", userSchema);

export default UnverifiedUser;