import mongoose from "mongoose";
import { AccountInterface } from "../@types/types";

export interface AccountModel extends mongoose.Document, AccountInterface {}

const accountSchema = new mongoose.Schema({
  email: {
    type: String,
    required: [true, "Email is Missing"],
    unique: true,
  },
  websitePassword: {
    type: String,
    required: [true, "Password is Missing"],
  },
  website: {
    type: String,
    required: [true, "Website is missing"],
  },
  websitePhotoUrl: {
    type: String,
    required: [true, "Website Picture is missing"],
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User is missing"],
  },
});

const Account = mongoose.model("Account", accountSchema);
export default Account;
