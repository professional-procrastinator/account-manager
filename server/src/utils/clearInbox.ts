import axios from "axios";
import config from "../config";
const mpUrl = config.mailServer;

async function clearInbox(mail: string, domain: string, token: string) {
  const res = await axios.delete(`${mpUrl}/mails/${domain}/${mail}`, {
    headers: { "X-Mailserver-Token": token },
  });

  console.log(res);
  if (res.data.success) {
    return { success: true };
  } else {
    return { success: false };
  }
}

export default clearInbox;
