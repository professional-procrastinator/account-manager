import CryptoJS from "crypto-js";

function encrypt(msg: string, key2: boolean = false) {
  var salt = CryptoJS.lib.WordArray.random(128 / 8);

  var key = CryptoJS.PBKDF2(
    key2 ? process.env.HASH_KEY2! : process.env.HASH_KEY!,
    salt,
    {
      keySize: 256 / 32,
      iterations: 100,
    }
  );

  var iv = CryptoJS.lib.WordArray.random(128 / 8);

  var encrypted = CryptoJS.AES.encrypt(msg, key, {
    iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  });

  var transitmessage = salt.toString() + iv.toString() + encrypted.toString();
  return transitmessage;
}

function decrypt(data: string, key2: boolean = false) {
  const salt = CryptoJS.enc.Hex.parse(data.substr(0, 32));
  const iv = CryptoJS.enc.Hex.parse(data.substr(32, 32));
  const encrypted = data.substring(64);

  const key = CryptoJS.PBKDF2(
    key2 ? process.env.HASH_KEY2! : process.env.HASH_KEY!,
    salt,
    {
      keySize: 256 / 32,
      iterations: 100,
    }
  );

  const decrypted = CryptoJS.AES.decrypt(encrypted, key, {
    iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  }).toString(CryptoJS.enc.Utf8);

  return decrypted;
}

function encryptWithKey(msg: string, encryptKey: string) {
  var salt = CryptoJS.lib.WordArray.random(128 / 8);

  var key = CryptoJS.PBKDF2(encryptKey, salt, {
    keySize: 256 / 32,
    iterations: 100,
  });

  var iv = CryptoJS.lib.WordArray.random(128 / 8);

  var encrypted = CryptoJS.AES.encrypt(msg, key, {
    iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  });

  var transitmessage = salt.toString() + iv.toString() + encrypted.toString();
  return transitmessage;
}

function decryptWithKey(data: string, encryptKey: string) {
  const salt = CryptoJS.enc.Hex.parse(data.substr(0, 32));
  const iv = CryptoJS.enc.Hex.parse(data.substr(32, 32));
  const encrypted = data.substring(64);

  const key = CryptoJS.PBKDF2(encryptKey, salt, {
    keySize: 256 / 32,
    iterations: 100,
  });

  const decrypted = CryptoJS.AES.decrypt(encrypted, key, {
    iv,
    padding: CryptoJS.pad.Pkcs7,
    mode: CryptoJS.mode.CBC,
  }).toString(CryptoJS.enc.Utf8);

  return decrypted;
}

export { encrypt, decrypt, encryptWithKey, decryptWithKey };
