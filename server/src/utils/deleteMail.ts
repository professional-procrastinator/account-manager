import axios from "axios";
import config from "../config";
const mpUrl = config.mailServer;

async function deleteMail(
  uid: string,
  mail: string,
  domain: string,
  token: string
) {
  const res = await axios.delete(`${mpUrl}/mails/${domain}/${mail}/${uid}`, {
    headers: { "X-Mailserver-Token": token },
  });

  if (res.data.success) {
    return { success: true, mails: res.data.mails };
  } else {
    return { success: false };
  }
}

export default deleteMail;
