import axios from "axios";
import config from "../config";
const mpUrl = config.mailServer;

async function readInbox(mail: string, domain: string, token: string) {
  const res = await axios.get(`${mpUrl}/mails/${domain}/${mail}`, {
    headers: { "X-Mailserver-Token": token },
  });

  if (res.data.success) {
    return { success: true, mails: res.data.mails };
  }else{
    return {success: true, mails: []}
  }
}

export default readInbox;
