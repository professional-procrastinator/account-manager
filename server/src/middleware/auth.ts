import jwt from "jsonwebtoken";
import { NextFunction, Response } from "express";

function auth(req: any, res: Response, next: NextFunction) {
  const token = req.cookies.token;
  
  if (!token)
    return res.send({
      success: false,
      message: "Token not Provided (token)",
      cookies: req.cookies,
    });

  try {
    let decoded = jwt.verify(token, process.env.JWT_KEY!);
    req.user = decoded;
    next();
  } catch (e) {
    res.send({ success: false, message: "Invalid Token" });
  }
}

export default auth;
