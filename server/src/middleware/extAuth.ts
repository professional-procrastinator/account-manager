import User from "../models/user";
import { decryptWithKey, decrypt } from "../utils/hashing";
import { NextFunction, Response } from "express";

async function extAuth(req: any, res: Response, next: NextFunction) {
  const key = req.headers["x-arcula-extension-security-auth"];

  if (!key || key == "undefined")
    return res.send({
      success: false,
      message: "Token not Provided (key)",
      cookies: req.cookies,
    });

  try {
    const userData = await User.findById(req.user.id)
      .populate("identities")
      .lean()
      .select("-password");
    console.log(userData);
    if (!userData)
      return res.send({ success: false, message: "Invalid Token" });
    const decrypted = decryptWithKey(userData._mpHash, decrypt(key));

    if (!decrypted)
      return res.send({ success: false, message: "Invalid Token" });
      
    console.log(decrypted);
    req.mp = decrypted;
    req.userData = userData;
    next();
  } catch (e) {
    res.send({ success: false, message: "Invalid Token" });
  }
}

export default extAuth;
