//config/index.ts

const config = {
  PORT: process.env.PORT || 5000,
  MONGODB_URI: process.env.MONGODB_URI,
  MONGODB_TEST_URI: process.env.MONGODB_TEST_URI,
  allowedOrigins:
    process.env.NODE_ENV == "production"
      ? ["https://arcula.vercel.app"]
      : ["http://localhost:3000", "http://localhost:3001"],
  mailServer:
    process.env.NODE_ENV == "production"
      ? "http://34.71.138.141:8000"
      : "http://34.71.138.141:8000",
  cookieConfig:
    process.env.NODE_ENV == "production"
      ? {
          httpOnly: true,
          maxAge: 15552000000,
          secure: true,
          sameSite: "none",
        }
      : { httpOnly: true, maxAge: 15552000000 },
  frontendUrl:
    process.env.NODE_ENV == "production"
      ? "https://arcula.vercel.app"
      : "http://localhost:3000",
};

export default config;
