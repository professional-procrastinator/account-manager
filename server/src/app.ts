import dotenv from "dotenv";
dotenv.config();

import express from "express";
import cookieParser from "cookie-parser";
import cors from "cors";
import helmet from "helmet";

import config from "./config/index";

// routes
import auth from "./routes/auth";
import identities from "./routes/identities";
import mail from "./routes/mail";
import verify from "./routes/verify";

const app = express();

//test comment

app.use(helmet());

app.use(
  cors({
    // origin: (req.protocol + "://" + req.headers.host!) as string,
    origin: config.allowedOrigins,
    // origin(origin, cb) {
    //   cb(null, origin);
    // },
    methods: ["GET", "POST", "PUT", "DELETE"],
    allowedHeaders: [
      "Origin",
      "X-Requested-With",
      "Content-Type",
      "Accept",
      "x-api-key",
    ],
    credentials: true,
  })
);

// app.use(function (req, res, next) {
//   res.header(
//     "Access-Control-Allow-Origin",
//     "http://localhost:3000, http://localhost:3001"
//   );
//   res.header("Access-Control-Allow-Credentials", "true");
//   res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE");
//   res.header(
//     "Access-Control-Allow-Headers",
//     "Origin, X-Requested-With, Content-Type, Accept, x-api-key"
//   );
//   next();
// });

app.use(express.json({ limit: "50mb" }));
app.use(cookieParser());
app.use(express.urlencoded({ extended: true, limit: "50mb" }));

// app.use(() => console.log("here"));

app.get("/", (req, res) =>
  res.send({ env: process.env.NODE_ENV, allowedOrigins: config.allowedOrigins })
);

app.use("/auth", auth);
app.use("/identities", identities);
app.use("/mail", mail);
app.use("/verify", verify)
export default app;
