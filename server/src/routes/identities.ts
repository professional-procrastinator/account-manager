import { Router } from "express";
const router = Router();

import auth from "../middleware/auth";
import extAuth from "../middleware/extAuth";
import User, { UserModel } from "../models/user";
import Identity, { IdentityModel } from "../models/identity";
import axios from "axios";
import { generateSlug } from "random-word-slugs";
import { decrypt, encryptWithKey, decryptWithKey } from "../utils/hashing";
import bcrypt from "bcrypt";
import clearInbox from "../utils/clearInbox";
import { extractRootDomain } from "../utils/extractHostname";
import CryptoJS from "crypto-js";
import mongoose from "mongoose";

function randomIntFromInterval(min: number, max: number) {
  // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function capitalize(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function isUrl(s: string) {
  var regexp =
    /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
  return regexp.test(s);
}

router.get("/generateId", auth, async (req: any, res) => {
  var generateNew = true;

  var id;

  while (generateNew) {
    const slug = generateSlug(2, { format: "camel" });
    const num = randomIntFromInterval(1, 99);
    id = (slug + num).toLowerCase();

    const doesExist = await Identity.findOne({ email: id }).lean();
    if (doesExist) continue;
    generateNew = false;
  }

  const newUser = await User.findOneAndUpdate(
    { _id: req.user.id },
    { selectedId: id },
    { new: true }
  );

  res.send({ id });
});

router.post("/", auth, async (req: any, res) => {
  try {
    const user: UserModel = await User.findById(req.user.id).lean();
    const websiteUrl = req.body.websiteUrl;

    if (!user || !websiteUrl || !isUrl(websiteUrl) || !websiteUrl.includes("."))
      return res.send({ success: false, message: "Invalid Website URL" });

    const doesIdExist: IdentityModel | null = await Identity.findOne({
      email: user.selectedId,
    });

    if (doesIdExist)
      return res.send({ success: false, message: "Email ID already exists!" });

    // const result = await axios.post(
    //   'http://server.altemail.co/mail',
    //   {
    //     id: user.selectedId,
    //   },
    //   { headers: { 'x-api-key': process.env.API_KEY! } }
    // );
    // console.log('thing' + result);

    // if (result.data.err)
    //   return res.send({
    //     success: false,
    //     message: 'An error occured',
    //     err: result.data.err,
    //   });

    let iconsResult;
    try {
      iconsResult = await axios.get(
        `https://besticon.herokuapp.com/allicons.json?url=${websiteUrl}`
      );
    } catch (e) {
      iconsResult = { data: { icons: [{ width: 40, url: "/internet.png" }] } };
    }

    var finalWebsiteUrl = "/internet.png";
    var maxSize = 0;

    const icons = iconsResult.data.icons;

    icons.forEach((i: any) => {
      if (i.width > maxSize) {
        finalWebsiteUrl = i.url;
        maxSize = i.width;
      }
    });

    const newIdentity = await Identity.create({
      email: user.selectedId,
      // mailPassword: encrypt(result.data.password, true),
      website: websiteUrl,
      websitePhotoUrl: finalWebsiteUrl,
      user: user._id,
    });

    const updatedUser = await User.findByIdAndUpdate(user._id, {
      $push: { identities: newIdentity._id },
    });

    res.send({ success: true });
  } catch (e) {
    res.send({ success: false, message: "An error occured" });
  }
});

router.get("/", auth, async (req: any, res) => {
  try {
    // const user: UserModel = await User.findById(req.user.id)
    //   .lean()
    //   .populate("identities", "Identity");

    // if (!user) return res.send({ success: false, message: "An error occured" });

    const uIdentities = await Identity.find({ user: req.user.id }).lean();

    const identities: any = [];

    // @ts-ignore
    uIdentities.forEach((id) => {
      // @ts-ignore
      identities.push({
        // @ts-ignore
        ...id,
        // @ts-ignore
        websitePassword: id.websitePassword
          ? // @ts-ignore
            id.websitePassword.length == 0
            ? false
            : true
          : false,
      });
    });

    res.send({ success: true, identities: identities });
  } catch (e) {
    res.send({ succes: false, message: "An error occured" });
  }
});

router.delete("/:id", auth, async (req: any, res) => {
  try {
    const user: UserModel = await User.findById(req.user.id).lean();
    const identity: IdentityModel = await Identity.findById(
      req.params.id
    ).lean();

    if (!user || !identity)
      return res.send({ success: false, message: "Can't Delete!" });

    if (!user.identities.some((id) => id?.toString() == identity._id))
      return res.send({ success: false, message: "Can't Delete!" });

    // TODO - Clear Inbox Before Deleting

    await Identity.findByIdAndDelete(identity._id);
    await User.findByIdAndUpdate(user._id, {
      $pull: { identities: identity._id },
    });
    res.send({ success: true });
  } catch (e) {
    res.send({ success: false, message: "An error occured", e });
  }
});

router.post("/:id/password", auth, async (req: any, res) => {
  try {
    const user: UserModel = await User.findById(req.user.id).lean();
    const identity: IdentityModel = await Identity.findById(
      req.params.id
    ).lean();
    const master = req.body.masterPassword;
    const password = req.body.password;

    if (!user || !identity || !master || !password || !user.masterPassword)
      return res.send({ success: false, message: "An error occured" });

    console.log(new mongoose.Types.ObjectId(identity._id), "User");
    //@ts-ignore
    if (!user.identities.some((a) => a.toString() == identity._id))
      return res.send({
        success: false,
        message: "You can't change password for this Identity",
      });

    const match = await bcrypt.compare(master, user.masterPassword);

    if (!match)
      return res.send({ success: false, message: "Incorrect Master Password" });

    const encryptKey = CryptoJS.SHA256(master).toString();
    // console.log(encryptKey);
    const encryptedPswd = encryptWithKey(password, encryptKey);

    const iden = await Identity.findByIdAndUpdate(identity._id, {
      websitePassword: encryptedPswd,
    });

    res.send({
      //   mp: decrypt(encryptKey),
      success: true,
    });
  } catch (e) {
    res.send({ success: false, message: "An Error occured", e });
  }
});

router.get("/extension/:url(*)", auth, extAuth, async (req: any, res) => {
  try {
    const user = req.userData;

    if (!user) return res.send({ success: false });

    const mp = req.mp;

    const correctMp = await bcrypt.compare(mp, user.masterPassword);
    if (!correctMp)
      return res.send({ success: false, message: "Incorrect Master Password" });

    const queryUrl = extractRootDomain(req.params.url);
    const idExists: any = [];
    const key = CryptoJS.SHA256(mp).toString();

    user.identities.forEach((id: any) => {
      const urlToCompare = extractRootDomain(id.website);
      if (urlToCompare.toLowerCase() == queryUrl.toLowerCase()) {
        const pswd = id.websitePassword
          ? decryptWithKey(id.websitePassword, key)
          : null;
        idExists.push({ ...id, websitePassword: pswd });
      }
    });
    res.send({ success: true, identities: idExists });
  } catch (e) {
    console.log(e);
    res.send({ success: false });
  }
});

export default router;
