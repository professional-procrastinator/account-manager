import {
  Router,
  Request,
  Response,
  NextFunction,
  CookieOptions,
} from "express";
import User, { UserModel, UserInterface } from "../models/user";
import auth from "../middleware/auth";
import extAuth from "../middleware/extAuth";
import generateRandomString from "../utils/generateRandomString";
import { AvatarGenerator } from "random-avatar-generator";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import CryptoJS from "crypto-js";
const mongoose = require("mongoose");
import { encryptWithKey, encrypt } from "../utils/hashing";
import PassId from "../models/passid";
import UnverifiedUser from "../models/unverifieduser";
import config from "../config";
import { ConfigSet } from "ts-jest";
const cookieConfig = config.cookieConfig;

// mail imports
const nodemailer = require("nodemailer");
const hbs = require("nodemailer-express-handlebars");

//make handlebar options for template
const handleBarOptions = {
  viewEngine: {
    extName: ".html",
    partialsdir: "./src/views",
    defaultLayout: false,
  },
  viewPath: "./src/views",
  extName: ".handlebars",
};

//transporter
const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "verify.arcula@gmail.com",
    pass: "arculapassword",
  },
});

//midleware for transporter
transporter.use("compile", hbs(handleBarOptions));

//universal mail options
const universalMailOptions = {
  from: "verify.arcula@gmail.com",

  subject: "Verify your email address",
  text: "verify your email address to access the arcula dashboard",
  template: "verify",
  url: config.frontendUrl
};

const router = Router();

const validateEmail = (email: string) => {
  return String(email)
    .toLowerCase()
    .match(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
};

function checkEmail(req: Request, res: Response, next: NextFunction) {
  if (req.body.email.includes("@altemail.co")) {
    res.send({ success: false, message: "Invalid Email ID" });
  } else {
    next();
  }
}

router.post("/register", checkEmail, async (req: any, res) => {
  try {
    //   console.log(req);
    const newUser: UserInterface = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
      photoUrl: "",
      identities: [],
      accounts: [],
    };

    if (
      !newUser.email ||
      newUser.email == "" ||
      newUser.email.trim() == "" ||
      !newUser.name ||
      newUser.name == "" ||
      newUser.name.trim() == "" ||
      !newUser.password ||
      newUser.password == "" ||
      newUser.password.trim() == ""
    ) {
      return res.send({
        success: false,
        message: "All fields are required.",
      });
    }

    const validEmail = validateEmail(newUser.email);
    if (!validEmail)
      return res.send({ success: false, message: "Invalid Email" });

    const doesUserExist = await User.findOne({ email: newUser.email }).lean();
    const doesUnverifiedUserExist = await UnverifiedUser.findOne({
      email: newUser.email,
    }).lean();

    if (doesUserExist || doesUnverifiedUserExist)
      return res.send({
        success: false,
        message: "Account exists with this email",
      });

    const generator = new AvatarGenerator();
    const photoUrl = generator.generateRandomAvatar();

    const hashed = await bcrypt.hash(newUser.password, 15);

    newUser.photoUrl = photoUrl;
    newUser.password = hashed;

    const {
      name,
      email,
      photoUrl: pic,
      _id,
    } = await UnverifiedUser.create(newUser);

    //send mail

    transporter.sendMail(
      {
        ...universalMailOptions,
        to: email,
        context: {
          name: name,
          url: config.frontendUrl + "/verify/account" + _id,
        },
      },
      function (err: any, data: any) {
        if (err) {
          res.send({ success: false, message: "Error sending" });
        }
      }
    );
    const token = jwt.sign({ id: _id }, process.env.JWT_KEY!);
    // res.cookie('token');
    res.cookie("token", token, cookieConfig as CookieOptions).send({
      success: true,
      data: { name, email, photoUrl: pic, _id, masterPassword: false },
    });
  } catch (e) {
    res.send({ success: false, message: "An error occured" });
  }
});

router.post("/login", checkEmail, async (req, res) => {
  try {
    const email = req.body.email;
    const pswd = req.body.password;

    const user: UserModel = await User.findOne({ email }).lean();

    if (!user) {
      const unverified = await UnverifiedUser.findOne({ email }).lean();
      if (unverified) {
        return res.send({
          success: false,
          message: "You need to verify your account first",
        });
      } else {
        return res.send({ success: false, message: "Invalid Credentials" });
      }
    }

    const match = await bcrypt.compare(pswd, user.password);
    if (!match)
      return res.send({ success: false, message: "Invalid Credentials" });

    const token = jwt.sign({ id: user._id }, process.env.JWT_KEY!);

    const finalUserToSendOK = await User.findOne({ email: email })
      .lean()
      .select("-password -identities");
    res.cookie("token", token, cookieConfig as CookieOptions).send({
      success: true,
      user: {
        ...finalUserToSendOK,
        masterPassword: finalUserToSendOK.masterPassword
          ? finalUserToSendOK.masterPassword?.length == 0
            ? false
            : true
          : false,
      },
    });
  } catch (e) {
    res.send({ success: false, message: "An error occured" });
  }
});

router.post("/extension/login", checkEmail, async (req, res) => {
  try {
    const email = req.body.email;
    const password = req.body.password;
    const mp = req.body.masterPassword;

    const user: UserModel = await User.findOne({ email: email }).lean();

    if (!user) {
      const unverified = await UnverifiedUser.findOne({ email }).lean();
      if (unverified) {
        return res.send({
          success: false,
          message: "You need to verify your account first",
        });
      } else {
        return res.send({ success: false, message: "Invalid Credentials" });
      }
    }

    if (!user.masterPassword)
      return res.send({
        success: false,
        message: "You haven't set a master password yet.",
      });

    const pswdMatch = await bcrypt.compare(password, user.password);
    if (!pswdMatch)
      return res.send({ success: false, message: "Invalid Credentials" });

    const mpMatch = await bcrypt.compare(mp, user.masterPassword);
    console.log(mp, user.masterPassword, mpMatch);
    if (!mpMatch)
      return res.send({ success: false, message: "Invalid Credentials" });

    const string = generateRandomString(16);
    const key = CryptoJS.SHA256(string);
    const hashedMp = encryptWithKey(mp, key.toString());

    await User.findOneAndUpdate({ _id: user._id }, { _mpHash: hashedMp });

    const authToken = jwt.sign({ id: user._id }, process.env.JWT_KEY!);

    res
      .cookie("token", authToken, cookieConfig as CookieOptions)
      .send({ success: true, key: encrypt(key.toString()) });
  } catch (e) {
    console.log(e);
    res.send({ success: false, message: "An error occured" });
  }
});

router.get("/extension/me", auth, extAuth, async (req: any, res) => {
  try {
    if (!req.user)
      return res.send({ success: false, message: "Invalid Session (token)" });

    if (!req.mp)
      return res.send({ success: false, message: "Invalid Session (_map)" });

    console.log(req.mp.mp);

    const user = req.userData;

    if (!user) return res.send({ success: false, message: "Invalid Session" });

    res.send({
      success: true,
      user: {
        _id: user._id,
        name: user.name,
        email: user.email,
        photoUrl: user.photoUrl,
        selectedId: user.selectedId,
        masterPassword: user.masterPassword
          ? user.masterPassword?.length == 0
            ? false
            : true
          : false,
      },
    });
  } catch (e) {
    return res.send({ success: false, message: "An error occured" });
  }
});

router.get("/me", auth, async (req: any, res) => {
  try {
    if (!req.user)
      return res.send({ success: false, message: "Invalid Session" });

    const user = await User.findOne({ _id: req.user.id })
      .lean()
      .select("-password -identities -accounts");

    const user2 = await UnverifiedUser.findOne({ _id: req.user.id })
      .lean()
      .select("-password -identities -accounts");
    // console.log(user2);

    if (!user && !user2)
      return res.send({ success: false, message: "Invalid Session" });
    if (!user) {
      console.log(user2);
      return res.send({
        success: true,
        user: {
          ...user2,
          masterPassword: user2.masterPassword
            ? user2.masterPassword?.length == 0
              ? false
              : true
            : false,
        },
      });
    }

    res.send({
      success: true,
      user: {
        ...user,
        masterPassword: user.masterPassword
          ? user.masterPassword?.length == 0
            ? false
            : true
          : false,
      },
    });
  } catch (e) {
    res.send({ success: false, message: "An Error Occured" });
  }
});

router.post("/logout", auth, (req, res) => {
  res.clearCookie("token").send({ success: true });
});

router.post("/extension/logout", auth, extAuth, (req, res) => {
  // res.setHeader("Set-Cookie", ["token1=;", "_map=;"]).send({ success: false });
  res.clearCookie("token").send({ success: true });
});

router.post("/masterPassword", auth, async (req: any, res) => {
  console.log("Hi");
  try {
    const pswd = req.body.masterPassword;
    const user = await User.findById(req.user.id).lean();

    if (!pswd || !user)
      return res.send({ success: false, message: "An error ocurred" });

    if (user.masterPassword && user.masterPassword.length >= 6) {
      return res.send({
        success: false,
        message: "User already has a master password",
      });
    }

    if (pswd.length < 6)
      return res.send({
        success: false,
        message: "Minimum Password length should be 6",
      });

    const hashed = await bcrypt.hash(pswd, 15);

    const newU = await User.findByIdAndUpdate(user._id, {
      masterPassword: hashed,
    });

    res.send({ success: true });
  } catch (e) {
    res.send({ success: false, message: "An error occured" });
  }
});

router.post("/password", async function (req: any, res) {
  console.log("here");
  try {
    console.log("here");
    const { uid, password, confirm } = req.body;
    console.log("gfdfgdgdf");
    const userUID = await PassId.findOne({ uid: uid });
    if (userUID) {
      if (password == confirm) {
        const hashed = await bcrypt.hash(password, 15);
        const user = await User.findOneAndUpdate(
          { _id: userUID.user },
          { password: hashed }
        );
        const deletedUID = await PassId.findOneAndDelete({ uid: uid });

        return res.send({ success: true });
      } else {
        return res.send({ success: false, message: "Passwords do not match" });
      }
    } else {
      return res.send({ success: false, message: "Invalid UID" });
    }
  } catch (e) {
    console.log(e);
    res.send({ success: false, message: "An error occured" });
  }
});

router.post("/verify", async function (req: any, res) {
  console.log(req.body);
  try {
    const id = req.body.id;
    console.log(id, mongoose.isValidObjectId("551137c2f9e1fac808a5f572"));
    if (!id || mongoose.Types.ObjectId.isValid(id) == false) {
      return res.send({ success: false, message: "invalid id" });
    }
    const unverifiedUser = await UnverifiedUser.findOne({ _id: id });

    if (!unverifiedUser) {
      return res.send({
        success: false,
        message: "User with this ID does not exist",
      });
    }
    const user = {
      name: unverifiedUser.name,
      email: unverifiedUser.email,
      photoUrl: unverifiedUser.photoUrl,
      verified: true,
      password: unverifiedUser.password,
    };

    const newUser = await User.create(user);
    const deletedUser = await UnverifiedUser.deleteOne({ _id: id });

    const token = jwt.sign({ id: newUser._id }, process.env.JWT_KEY!);
    return res
      .cookie("token", token, cookieConfig as CookieOptions)
      .send({ success: true, user: newUser });
  } catch (e) {
    return res.send({ success: false, message: "an error occured" });
  }
});

router.get("/resend/:id", async function (req: any, res) {
  if (mongoose.isValidObjectId(req.params.id) == false) {
    return res.send({ message: "Invalid ID", success: false });
  }
  const user = await UnverifiedUser.findOne({ _id: req.params.id });
  if (!user) {
    return res.send({ success: false, message: "Invalid User" });
  }
  transporter.sendMail({
    ...universalMailOptions,
    to: user.email,
    context: {
      name: user.name,
      url: config.frontendUrl + "/verify/account/" + user._id,
    },
  });

  res.send({ success: true });
});

export default router;
