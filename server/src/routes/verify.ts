import { Router } from "express";
import auth from "../middleware/auth";
const nodemailer = require("nodemailer");
import mongoose from "mongoose";
import PassId, { PassIdModel } from "../models/passid";
import path from "path";
const bcrypt = require("bcrypt");
import config from "../config";
import User from "../models/user";
import generateRandomStringNonSpecial from "../utils/generateRandomStringNoSpecialChars";
const hbs = require("nodemailer-express-handlebars");

const handleBarOptions = {
  viewEngine: {
    extName: ".html",
    partialsdir: "./src/views",
    defaultLayout: false,
  },
  viewPath: "./src/views",
  extName: ".handlebars",
};

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "verify.arcula@gmail.com",
    pass: "arculapassword",
  },
});

transporter.use("compile", hbs(handleBarOptions));

const universalMailOptions = {
  from: "verify.arcula@gmail.com",
  subject: "Reset your Arcula account's Password. ",
  text: "reset your Arcula account's Password",
  template: "password",
  url: config.frontendUrl + "/verify"
};

const router = Router();

const getRandomInt = () => {
  return Math.floor(Math.random() * 9);
};

const getUID = async () => {
  let validUID = false;
  let uid;
  while (validUID == false) {
    uid = generateRandomStringNonSpecial(15);
    const uidExists = await PassId.findOne({ uid: uid });
    if (uidExists) {
      continue;
    } else {
      validUID = true;
    }
  }
  return uid;
};

router.get("/send/:email(*)", async (req: any, res) => {
  const existingUID = await PassId.findOne({ userEmail: req.params.email });
  console.log(existingUID);
  if (existingUID == null) {
    const uid = await getUID();
    console.log(uid);
    const userData = await User.findOne({ email: req.params.email });
    console.log(req.params);
    if (!userData)
      return res.send({ success: false, message: "Account does not exist" });

    const userUID = await PassId.create({
      uid: uid,
      user: userData._id,
      userEmail: req.params.email,
    });

    let mailOptions = {
      from: universalMailOptions.from,
      subject: universalMailOptions.subject,
      text: universalMailOptions.text,
      template: universalMailOptions.template,
      to: userData.email,
      context: {
        name: userData.name,
        url: universalMailOptions.url + "/" + userUID.uid,
      },
    };
    transporter.sendMail(mailOptions);
    res.send({ success: true, message: "Email has been sent!" });
  } else {
    res.send({ success: false, message: "Email has already been sent!" });
  }
});

router.get("/resend/:email(*)", async (req: any, res) => {
  const existingUID = await PassId.findOne({ userEmail: req.params.email });
  if (existingUID == null) {
    const uid = await getUID();
    console.log(uid);
    const userData = await User.findOne({ email: req.params.email });

    if (!userData)
      return res.send({ success: false, message: "Account does not exist" });

    const userUID = await PassId.create({
      uid: uid,
      user: userData._id,
      userEmail: req.params.email,
    });

    let mailOptions = {
      from: universalMailOptions.from,
      subject: universalMailOptions.subject,
      text: universalMailOptions.text,
      template: universalMailOptions.template,
      to: userData.email,
      context: {
        name: userData.name,
        url: universalMailOptions.url + "/" + userUID.uid,
      },
    };
    transporter.sendMail(mailOptions);
    res.send({ success: true, message: "Email has been sent!" });
  } else {
    const deletedUID = await PassId.deleteOne({ userEmail: req.params.email });
    const uid = await getUID();
    console.log(uid);
    console.log(req.params.email);
    console.log(req.params.email, "asdsd");
    const userData = await User.findOne({ email: req.params.email });

    if (!userData)
      return res.send({ success: false, message: "Account does not exist" });

    const userUID = await PassId.create({
      uid: uid,
      user: userData._id,
      userEmail: req.params.email,
    });
    console.log("I am gaymr :sunglass:");
    let mailOptions = {
      from: universalMailOptions.from,
      subject: universalMailOptions.subject,
      text: universalMailOptions.text,
      template: universalMailOptions.template,
      to: userData.email,
      context: {
        name: userData.name,
        url: universalMailOptions.url + "/" + userUID.uid,
      },
    };
    transporter.sendMail(mailOptions);
    res.send({ success: true, message: "Email has been sent!" });
  }
});
router.post("/validate", async function (req: any, res) {
  console.log(req.body, "hi");
  const uid = req.body.uid;
  console.log(uid);
  const existingUid = await PassId.findOne({ uid: uid });
  console.log(existingUid);
    
  if (existingUid) {
    console.log("HERE");
    res.send({ success: true, valid: true, email: existingUid.userEmail });
  } else {
    res.send({ success: true, valid: false });
  }
});

export default router;
