import { Router } from "express";
import axios from "axios";

import auth from "../middleware/auth";
import User, { UserModel } from "../models/user";

import Identity, { IdentityModel } from "../models/identity";
import { decrypt } from "../utils/hashing";
import readInbox from "../utils/readInboxNew";
import clearInbox from "../utils/clearInbox";
import deleteMail from "../utils/deleteMail";

const router = Router();

router.get("/all", auth, async (req: any, res) => {
  console.log("here");
  const user: UserModel = await User.findById(req.user.id)
    .lean()
    .populate("identities");

  if (!user) return res.send({ success: false, message: "An error occured" });

  const identities = user.identities;

  const results: any = [];
  const mails: any = [];

  const result = await Promise.all(
    identities.map(async (idn) => {
      if (typeof idn == "string") return;
      const mailid = idn?.email;
      const mailBox = await readInbox(
        mailid!,
        idn?.domain || "altemail.co",
        req.cookies.token
      );
      results.push(mailBox);
    })
  );

  results.forEach((result: any) => {
    if (result.success == false) return;
    const keys = Object.keys(result.mails);

    keys.forEach((key: string) => {
      mails.push(result.mails[key]);
    });
  });

  //   console.log(result);
  res.send({ success: true, mails });
});

router.get("/:mail", auth, async (req: any, res) => {
  const user = await User.findById(req.user.id).lean();
  const identity = await Identity.findOne({ email: req.params.mail }).lean();

  if (!user || !identity)
    return res.send({ success: false, message: "An error occured" });

  if (!user.identities.some((id: any) => id.toString() == identity._id))
    return res.send({
      success: false,
      message: "You can't see inbox for this email",
    });

  const mailid = identity.email;

  console.log("Route log", mailid);

  const result = await readInbox(mailid, identity.domain, req.cookies.token);
  res.send({ ...(result as Object), mailid });
});

router.delete("/clearInbox/:mail", auth, async (req: any, res) => {
  const user = await User.findById(req.user.id).lean();

  if (!user) return res.send({ success: false, message: "Not authorised" });

  if (req.params.mail == "All") {
    const identities = await Identity.find({ user: user._id });

    const result = await Promise.all(
      identities.map(async (i) => {
        const mailid = i.email;

        const r = await clearInbox(mailid, i.domain, req.cookies.token);
        return r;
      })
    );
    res.send(result);
  } else {
    const identity = await Identity.findOne({ email: req.params.mail });

    if (!identity) return res.send({ success: false });

    if (identity.user != req.user.id)
      return res.send({ success: false, message: "Not authorised" });

    const mailid = identity.email;

    const result = await clearInbox(mailid, identity.domain, req.cookies.token);

    res.send([result]);
  }
});

router.delete("/deleteMail/:mail/:uid", auth, async (req: any, res) => {
  const user = await User.findById(req.user.id).lean();

  console.log(req.user, req.params.mail, req.params.uid);

  if (!user) return res.send({ success: false, message: "Not authorised" });

  const identity = await Identity.findOne({
    email: { $regex: new RegExp(`^${req.params.mail}$`), $options: "i" },
  }).lean();

  if (!identity) return res.send({ success: false });

  if (identity.user != req.user.id)
    return res.send({ success: false, message: "Not authorised" });

  const mailid = identity.email;

  const result = await deleteMail(
    req.params.uid,
    mailid,
    identity.domain,
    req.cookies.token
  );

  res.send(result);
});

export default router;
