import { useState, useEffect } from "react";
import Layout from "../components/Layout";

import styles from "../styles/pages/inbox.module.scss";
import TextField from "../components/TextField";
import SearchBar from "../modules/Inbox/SearchBar";
import SelectInbox from "../modules/Inbox/SelectInbox";
import Refresh from "../public/icons/refresh.svg";
import { useMediaQuery } from "react-responsive";
import axios from "../utils/axios";
import Mail from "../modules/Inbox/Mail";
import Loader from "../components/Loader";
import showToast, { showErrorToast } from "../components/Toast";
import TextButton from "../components/Button/Text";
export default function Inbox() {
  const [chosenIdentity, setChosenIdentity] = useState("All");
  const [identities, setIdentities] = useState([]);
  const [loading, setLoading] = useState(true);
  const [searchTerm, setSearchTerm] = useState("");
  const [isHeaderVisible, setHeaderVisible] = useState(true);

  const [refresh, setRefresh] = useState(false);
  const [mailLoading, setMailLoading] = useState(false);
  const [inboxCleared, setInboxCleared] = useState(false);
  const [mailDeleted, setMailDeleted] = useState(false);
  const phone = useMediaQuery({ maxWidth: "651px" });
  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const identity = urlParams.get("mail");

    if (identities) {
      console.log("got " + identity);
      if (identity) {
        console.log("got " + identity);
        console.log(identities);
        var found = identities.find(
          (i) => i.email.toLowerCase() === identity.toLowerCase()
        );

        if (found) {
          return setChosenIdentity(found.email);
        }

        return setChosenIdentity("All");
      }
    }
  }, [identities]);

  useEffect(async () => {
    await getIdentities();
    setLoading(false);
  }, []);

  async function clearInbox() {
    setMailLoading(true);
    const res = await axios.delete(`/mail/clearInbox/${chosenIdentity}`);
    let successful = true;
    res.data.forEach(({ success }) => {
      if (!success) successful = false;
    });
    setMailLoading(false);
    if (successful) {
      setInboxCleared(!inboxCleared);
      setRefresh(!refresh);
    } else {
      showErrorToast("An error Occured");
    }
    // showToast("Inbox Cleared!");
  }

  async function deleteMail(uid, mail) {
    // showErrorToast("An error Occured");
    console.log("yo", uid, mail);
    setMailLoading(true);
    const res = await axios.delete(
      `/mail/deleteMail/${mail.substring(0, mail.length - 12)}/${uid}`
    );
    setMailLoading(false);
    if (res.data.success) {
      setMailDeleted(!mailDeleted);
      setRefresh(!refresh);
    } else {
      showErrorToast("An error Occured");
    }
  }

  const getIdentities = async () => {
    const { data } = await axios.get("/identities");

    if (data.success) {
      // data.identities.forEach((identity) => {
      //   setIdentities((identities) => {
      //     return [...identities, { identity }];
      //   });
      // });
      // console.log("Fetch identities, ", data.identities);
      // console.log("default identities, ", identities);
      setIdentities((ids) => data.identities);
    }
  };

  if (loading) {
    return (
      <div className="center" style={{ height: "100vh" }}>
        <Loader />
      </div>
    );
  }

  return (
    <Layout title="Inbox | Arcula">
      <div className={styles.inbox}>
        {isHeaderVisible && (
          <div className={styles.inbox__header}>
            <div className={styles.inbox__header__top}>
              <div className={styles.inbox__header__top__heading}>Inbox</div>

              <div className={styles.inbox__header__top__actions}>
                <TextButton
                  className={styles.inbox__header__top__actions__button}
                  click={clearInbox}
                >
                  Clear Inbox
                </TextButton>
                {!phone ? (
                  <TextButton
                    className={styles.inbox__header__top__actions__button}
                    click={() => setRefresh(!refresh)}
                  >
                    Refresh
                    <Refresh style={{ height: "20px", marginLeft: "4px" }} />
                  </TextButton>
                ) : (
                  <TextButton
                    className={styles.inbox__header__top__actions__button}
                    onClick={() => setRefresh(!refresh)}
                    style={{ marginLeft: "-4px" }}
                  >
                    <Refresh style={{ height: "20px", marginLeft: "0px" }} />
                  </TextButton>
                )}
              </div>
            </div>

            <div className={styles.inbox__header__bottom}>
              {!phone ? (
                <div className={styles.inbox__header__bottom__searchbar}>
                  <SearchBar
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                  />
                </div>
              ) : (
                <div className={styles.inbox__header__bottom__phone__searchbar}>
                  <SearchBar
                    value={searchTerm}
                    onChange={(e) => setSearchTerm(e.target.value)}
                  />
                </div>
              )}

              <SelectInbox
                identities={identities}
                chosenIdentity={chosenIdentity}
                setChosenIdentity={setChosenIdentity}
              />
            </div>
          </div>
        )}
        <div className={styles.inbox__body}>
          <Mail
            identities={identities}
            chosenIdentity={chosenIdentity}
            refresh={refresh}
            mailLoading={mailLoading}
            inboxCleared={inboxCleared}
            deletedMail={mailDeleted}
            searchTerm={searchTerm}
            setHeaderVisible={setHeaderVisible}
            onDeleteMail={deleteMail}
          />
        </div>
      </div>
    </Layout>
  );
}
