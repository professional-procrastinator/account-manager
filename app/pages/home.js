import Layout from "../components/Layout";
import Home from "../modules/Home";

export default function HomePage() {
  return (
    <Layout title="Home | Arcula">
      <Home />
    </Layout>
  );
}
