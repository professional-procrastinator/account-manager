import axios from "../../utils/axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Loader from "../../components/Loader";
import styles from "./index.module.scss";
import classNames from "classnames/bind";
import Primary from "../../components/Button/Primary";
import TextField from "../../components/TextField";
import Layout from "../../components/Layout";
import Error from "../../components/Error";
const cx = classNames.bind(styles);

const ForgotPassword = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(true);
  const [valid, setValid] = useState(false);
  const [uid, setUid] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  const [confirm, setConfirm] = useState("");
  const [loading2, setLoading2] = useState(false);

  async function getValid(uid) {
    const data = axios.post(`/verify/validate`, {
      uid: uid,
    });
    return data;
  }
  async function changePass() {
    setLoading2(true);
    const data = await axios.post("/auth/password", {
      uid: uid,
      password: pass,
      confirm: confirm,
    });
    setLoading2(false);
    router.push("/home");
  }
  useEffect(async () => {
    if (router.asPath !== router.route) {
      let { uid } = router.query;
      const data = await getValid(uid);
      setUid(uid);
      setEmail(data.data.email);
      console.log(data.data);
      setValid(data.data.valid);
      setLoading(false);
    }
  }, [router]);
  console.log(valid);
  if (loading) {
    return (
      <div className="center" style={{ height: "100vh" }}>
        <Loader />
      </div>
    );
  } else {
    if (valid) {
      return (
        <Layout title="Reset Your Password">
          <div className={cx("special")}>
            <div className={cx("container")}>
              <div className={cx("container__header")}>
                <h1 className={cx("container__header__heading")}>
                  Reset Password
                </h1>
                <p>Reset Password for {email}</p>
              </div>

              <form
                className={cx("container__content__form")}
                onSubmit={async (e) => {
                  e.preventDefault();
                  const data = await changePass();
                  console.log(data);
                }}
              >
                <div className={cx("container__content__form__field")}>
                  <label
                    className={cx("container__content__form__field__label")}
                  >
                    New Password
                  </label>

                  <TextField
                    className={cx("container__content__form__field__input")}
                    type="email"
                    password={true}
                    value={pass}
                    onChange={(e) => {
                      setPass(e.target.value);
                    }}
                    required
                  />
                </div>

                <div className={cx("container__content__form__field")}>
                  <label
                    className={cx("container__content__form__field__label")}
                  >
                    Confirm Password
                  </label>

                  <TextField
                    className={cx("container__content__form__field__input")}
                    type="password"
                    value={confirm}
                    password={true}
                    onChange={(e) => {
                      setConfirm(e.target.value);
                    }}
                    required
                    minLength={8}
                  />
                </div>
                <div className={cx("container__content__form__error")}>
                  <p></p>
                </div>
                <div className={cx("container__content__form__footer")}>
                  <Primary
                    className={cx(
                      "container__content__form__footer__submit",
                      {}
                    )}
                    click={changePass}
                    loading={loading2}
                  >
                    {loading2 ? "Resetting..." : "Reset"}
                  </Primary>
                </div>
              </form>
            </div>
          </div>
        </Layout>
      );
    } else {
      return (
        <Layout>
          <div className="center" style={{ height: "calc(100vh - 200px)" }}>
            <Error>Invalid or Expired Code</Error>
          </div>
        </Layout>
      );
    }
  }
};

export default ForgotPassword;
