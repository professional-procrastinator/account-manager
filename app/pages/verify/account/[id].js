import axios from "../../../utils/axios";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import Layout from "../../../components/Layout";
import Loader from "../../../components/Loader";
import Error from "../../../components/Error";
import Secondary from "../../../components/Button/Secondary";
import useSession from "../../../utils/hooks/useSession";

const VerifyMail = () => {
  const router = useRouter();

  const verify = async (id) => {
    const data = await axios.post("/auth/verify", {
      id: id,
    });
    const response = await data.data;
    return response;
  };

  const resendMail = () => {};

  useEffect(async () => {
    if (router.asPath !== router.route) {
      let { id } = router.query;
      const data = await verify(id);
      if (data.success) {
        // router.push("/home");
        window.location = "/home";
      } else {
        // router.push("/login");
        window.location = "/login";
      }
    }
  }, [router]);

  return (
    <div className="center" style={{ height: "100vh" }}>
      <Loader />
    </div>
  );
};
export default VerifyMail;
