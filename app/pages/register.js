import { useState, useContext } from "react";
import { useRouter } from "next/router";
import useSession from "../utils/hooks/useSession";
import Layout from "../components/Layout/";

import RegisterForm from "../modules/Auth/Register";

import styles from "../styles/pages/login.module.scss";

export default function Register() {
  return (
    <Layout title="Register | Arcula">
      <div className={styles.center}>
        <RegisterForm />
      </div>
    </Layout>
  );
}
