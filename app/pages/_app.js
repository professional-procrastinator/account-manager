import "../styles/globals.scss";

import { UserProvider as UserContextProvider } from "../utils/context/user";
import { ToastContainer, Bounce } from "react-toastify";
// import Auth from "../components/Auth";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <UserContextProvider>
        <Component {...pageProps} />
      </UserContextProvider>
      <ToastContainer
        position="bottom-right"
        transition={Bounce}
        limit={1}
        closeOnClick={false}
        // closeButton={false}
      />
    </>
  );
}

export default MyApp;
