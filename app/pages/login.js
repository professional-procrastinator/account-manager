import { useRouter } from "next/router";
import useSession from "../utils/hooks/useSession";
import Layout from "../components/Layout/";

import LoginForm from "../modules/Auth/Login";

import styles from "../styles/pages/login.module.scss";
import { useState, useRef } from "react";
import useOnClickOutside, { Popup } from "../components/Popup";
import MailForgot from "../modules/Auth/Login/MailPopop";

export default function Login() {
  const [forgot, setForgot] = useState(false);
  const [mail, setMail] = useState(false);
  const forgotPopupRef = useRef(null);
  useOnClickOutside(forgotPopupRef, function () {
    setForgot(false)
  })
  const  mailPopupRef = useRef(null);
  return (
    <Layout title="Login | Arcula">
      <div className={styles.center}>
        <LoginForm  forgot={setForgot}/>
      <Popup ref={forgotPopupRef} popupState={forgot}>
        <MailForgot>

        </MailForgot>

      </Popup>
      </div>
    </Layout>
  );
}
