import Layout from "../components/Layout";
import useSession from "../utils/hooks/useSession";
import styles from "../styles/pages/profile.module.scss";
import Danger from "../components/Button/Danger";
import { useRouter } from "next/router";
import PrimaryButton from "../components/Button/Primary";
import Secondary from "../components/Button/Thirdary";
import { useEffect, useState, useRef } from "react";
import { Popup, useOnClickOutside } from "../components/Popup";
import Loader from "../components/Loader";
import classNames from "classnames/bind";
import SetMaster from "../modules/Home/Password/Set";
import { ChangePass } from "../components/ChangePswd";
const cx = classNames.bind(styles);

export default function Profile() {
  const { updateUser, user, logout } = useSession();
  const router = useRouter();
  const [master, setMaster] = useState(false);
  const [loading, setLoading] = useState(false);
  const changePopupRef = useRef(null);
  const [popupRef, setPopupRef] = useRef(null);
  const [changePswdOpen, setChangePswdOpen] = useState(false);
  const [masterPswdOpen, setMasterPswdOpen] = useState(false);
  useOnClickOutside(changePopupRef, () => setChangePswdOpen(false));

  useEffect(async () => {
    const updatedUser = await updateUser();
    setMaster(updatedUser.masterPassword);
    console.log(updatedUser);
  }, [loading]);

  return (
    <Layout title="Profile | Arcula">
      {!user && (
        <div className="center">
          <Loader />
        </div>
      )}
      {user && (
        <>
          <div className={styles.profile}>
            <div className={styles.profile__avatar}>
              <img
                src={user.photoUrl}
                className={styles.profile__avatar__src}
              />
            </div>

            <div className={styles.profile__info}>
              <h2 className={styles.profile__info__name}>{user.name}</h2>
              <p className={styles.profile__info__email}>{user.email}</p>
            </div>
            {!loading ? (
              <div className={styles.profile__masterbanner}>
                <p className={styles.profile__masterbanner__para}>
                  {master
                    ? "Congrats! You have successfully set a Master Password"
                    : "You haven't added a Master Password yet. Add one to add passwords to your identities that autofill into websites with the Browser Extension."}
                </p>
                {!master && <PrimaryButton
                  click={() => {
                    setMasterPswdOpen(true);
                  }}
                  className={styles.profile__masterbanner__button}
                >
                  Add
                </PrimaryButton>}
              </div>
            ) : null}
            <div className={styles.profile__logout}>
              <Secondary click={(e) => setChangePswdOpen(true)}>
                Change Password
              </Secondary>
              <Danger
                onClick={logout}
                className={styles.profile__logout__button}
              >
                Logout
              </Danger>
            </div>
          </div>
          <Popup ref={popupRef} popupState={masterPswdOpen}>
            <SetMaster
              close={() => {
                setMasterPswdOpen(false);
              }}
              state={masterPswdOpen}
              reload={setLoading}
              reloadValue={loading}
              setMaster={setMaster}
            />
            <Popup ref={changePopupRef} popupState={changePswdOpen}>
              <ChangePass />
            </Popup>
          </Popup>
        </>
      )}
    </Layout>
  );
}
