import Layout from "../components/Layout/";
import Landing from "../components/Landing";

const Home = () => {
  return (
    <Layout title="Arcula - Treasure Your Privacy">
      <Landing/>
    </Layout>
  );
};

export default Home;
