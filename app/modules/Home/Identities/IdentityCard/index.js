import { useState, useEffect, useRef } from "react";
import { Popup, useOnClickOutside } from "../../../../components/Popup";
import styles from "./index.module.scss";
import useSession from "../../../../utils/hooks/useSession";
import MenuOverlay from "../../../../components/MenuOverlay";
import { useMediaQuery } from "react-responsive";
import AddPassword from "./Set";
import Thirdary from "../../../../components/Button/Thirdary";
import LockTick from "../../../../public/icons/lock_tick.svg";

export default function IdentityCard({ identity, reload, reloadValue }) {
  const [AddPasswordOpen, setAddPasswordOpen] = useState(false);
  const [MenuOverlayOpen, setMenuOverlayOpen] = useState(false);
  const { user } = useSession();
  const phoneOrNot = useMediaQuery({ query: "(max-width: 676px)" });
  const AddPasswordRef = useRef(null);
  useOnClickOutside(AddPasswordRef, () => setAddPasswordOpen(false));

  if (!phoneOrNot) {
    return (
      <div className={styles.main__identity}>
        <div className={styles.main__identity__imagecontainer}>
          <img
            src={identity.websitePhotoUrl}
            onError={({ currentTarget }) => {
              // e.src = "/internet.png"
              currentTarget.onerror = null;
              currentTarget.src = "/internet.png";
            }}
            style={{borderRadius: '6px'}}
            className={styles.main__identity__imagecontainer__image}
          />
        </div>
        <div className={styles.main__identity__text}>
          <h1 className={styles.main__identity__text__heading}>
            {identity.email}@altemail.co
          </h1>
          <h2 className={styles.main__identity__text__website}>
            {identity.website}
          </h2>
        </div>
        <div className={styles.main__identity__action}>
          {identity.websitePassword && (
            <LockTick
              style={{
                color: "#949494",
                height: "26px",
                marginRight: "12px",
              }}
            />
          )}
          {user.masterPassword ?
            (identity.websitePassword ? (
              <Thirdary
                className={styles.main__identity__action__button}
                click={() => {
                  setAddPasswordOpen(true);
                }}
              >
                Change Password
              </Thirdary>
            ) : (
              <Thirdary
                
                click={() => {
                  setAddPasswordOpen(true);
                }}
                className={styles.main__identity__action__button}
              >
                Add Password
              </Thirdary>
            ) ) : (
              <Thirdary className={styles.main__identity__action__disabled} disabled={true}>Add Password</Thirdary>
            )}
        </div>
        <MenuOverlay
          styles={styles}
          identity={identity}
          reload={reload}

          reloadValue={reloadValue}
        />
        {console.log(reloadValue)}
        <Popup ref={AddPasswordRef} popupState={AddPasswordOpen}>
          <AddPassword
            identity={identity}
            state={AddPasswordOpen}
            close={() => setAddPasswordOpen(false)}
            reload={reload}
            reloadValue={reloadValue}
          />
        </Popup>
      </div>
    );
  } else {
    return (
      <div className={styles.phone__identity}>
        <div className={styles.phone__identity__top}>
          <div className={styles.phone__identity__top__imagecontainer}>
            <img
              src={identity.websitePhotoUrl}
              onError={({ currentTarget }) => {
                // e.src = "/internet.png"
                currentTarget.onerror = null;
                currentTarget.src = "/internet.png";
              }}
              style={{borderRadius: "6px"}}
              className={styles.phone__identity__top__imagecontainer__image}
            />
          </div>
          <MenuOverlay
            styles={styles}
            phone={phoneOrNot}
            identity={identity}
            reload={reload}
            reloadValue={reloadValue}
          />
        </div>
        <div className={styles.phone__identity__text}>
          <h1 className={styles.phone__identity__text__heading}>
            {identity.email}@altemail.co
          </h1>
          <h2 className={styles.phone__identity__text__email}>
            {identity.website}
          </h2>
        </div>
        <div className={styles.phone__identity__action}>
          {identity.websitePassword && (
            <LockTick
              style={{
                color: "#949494",
                height: "26px",
                marginRight: "12px",
                marginLeft: "auto",
              }}
            />
          )}

{user.masterPassword ?
            (identity.websitePassword ? (
              <Thirdary style={{marginLeft: "0"}}
                className={styles.phone__identity__action__button}
                click={() => {
                  setAddPasswordOpen(true);
                }}
              >
                Change Password
              </Thirdary>
            ) : (
              <Thirdary
                className={styles.phone__identity__action__button}
                click={() => {
                  setAddPasswordOpen(true);
                }}
              >
                Add Password
              </Thirdary>
            ) ) : (
              <Thirdary className={styles.phone__identity__action__disabled} disabled={true}>Add Password</Thirdary>
            )}
        </div>
        <Popup ref={AddPasswordRef} popupState={AddPasswordOpen}>
          <AddPassword
            identity={identity}
            state={AddPasswordOpen}
            close={() => setAddPasswordOpen(false)}
            reload={reload}
            reloadValue={reloadValue}
          />
        </Popup>
      </div>
    );
  }
}
