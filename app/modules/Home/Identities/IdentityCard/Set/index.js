import { useState, useEffect } from "react";
import styles from "../../../../../styles/shared/popup.module.scss";
import Primary from "../../../../../components/Button/Primary";
import TextField from "../../../../../components/TextField";

import axios from "../../../../../utils/axios";
import showToast from "../../../../../components/Toast";
export default function AddPassword({
  identity,
  state,
  close,
  reload,
  reloadValue,
}) {
  const [masterPassword, setMasterPassword] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [adding, setAdding] = useState(false);

  useEffect(() => {
    if (!state) {
      setMasterPassword("");
      setPassword("");
      setError("");
      setAdding(false);
    }
  }, [state]);

  const add = async () => {
    setAdding(true);
    setError("");

    const { data } = await axios.post(`identities/${identity._id}/password`, {
      masterPassword,
      password,
    });

    if (data.success) {
      setAdding(false);
      close();
      showToast("Password Changed!");
      return reload(!reloadValue);
    }
    setAdding(false);
    return setError(data.message);
  };
  return (
    <form className={styles.main} onSubmit={(e) => {
      e.preventDefault()
      add()
    
    } }>
      <div className={styles.main__header}>
        <h2 className={styles.main__header__heading}>Add a Password</h2>
      </div>

      <div className={styles.main__body}>
        <div className={styles.main__body__form}>
          <div className={styles.main__body__form__field}>
            <label className={styles.main__body__form__field__label}>
              Master Password
            </label>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={masterPassword}
                onChange={(e) => {
                  setError("");
                  setMasterPassword(e.target.value);
                }}
                type="password"
                password={true}
                className={styles.main__body__form__field__content__input}
              />
            </div>
          </div>

          <div className={styles.main__body__form__field}>
            <label className={styles.main__body__form__field__label}>
              New Password ({identity.website})
            </label>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={password}
                onChange={(e) => {
                  setError("");
                  setPassword(e.target.value);
                }}
                type="password"
                password={true}
                className={styles.main__body__form__field__content__input}
              />
            </div>
          </div>
        </div>

        <div className={styles.main__body__error}>{error}</div>
      </div>

      <div className={styles.main__footer}>
        <Primary
          click={add}
          className={styles.main__footer__submit}
          loading={adding}
        >
          {adding ? "Adding..." : "Add Password"}
        </Primary>
      </div>
    </form>
  );
}
