import styles from "./index.module.scss";
import Add from "../../../public/icons/add.svg";
import Frowning from "../../../public/icons/Frowning.svg";
import { Popup, useOnClickOutside } from "../../../components/Popup";
import NewIdentityPopup from "./New";
import Image from "next/image";
import { useState, useEffect, useRef } from "react";
import axios from "../../../utils/axios";
import Dots from "../../../public/icons/dots.svg";
import useSession from "../../../utils/hooks/useSession";
import Primary from "../../../components/Button/Primary";
import MenuOverlay from "../../../components/MenuOverlay";
import Loader from "../../../components/Loader";
import SetMaster from "../Password/Set";
import { useMediaQuery } from "react-responsive";
import Add2 from "../../../public/icons/add2.svg";
import Error from "../../../components/Error";
import IdentityCard from "./IdentityCard";
// import showToast from "../../../components/Toast";
export default function Identities() {
  const [newAliasPopupOpen, setNewAliasPopupOpen] = useState(false);
  const [masterPswdOpen, setMasterPswdOpen] = useState(false);

  const aliasPopupRef = useRef(null);
  const masterPswdPopupRef = useRef(null);
  const [identities, setIdentities] = useState([]);
  const [hacky, setHacky] = useState(false);
  const [master, setMaster] = useState(false);
  const [dotDiv, setDotDiv] = useState(false);
  useOnClickOutside(aliasPopupRef, () => setNewAliasPopupOpen(false));
  useOnClickOutside(masterPswdPopupRef, () => setMasterPswdOpen(false));
  const { user, updateUser } = useSession();

  const [loading, setLoading] = useState(true);
  const isPhone = useMediaQuery({ query: "(max-width: 676px)" });
  useEffect(async () => {
    setLoading(true);
    console.log("loading");
    const { data } = await axios.get("/identities");
    const user_identities = data.identities;
    const updatedUser = await updateUser();
    setMaster(updatedUser.masterPassword);
    setIdentities(user_identities);
    setLoading(false);
  }, [hacky]);
  return (
    <div className={styles.container}>
      <div className={styles.container__header}>
        {!isPhone ? (
          <>
            <h1 className={styles.container__header__heading}>
              Your Identities
            </h1>
            <button
              className={styles.container__header__button}
              onClick={() => {
                setNewAliasPopupOpen(true);
              }}
            >
              <div>
                <Add />
              </div>
              {/* <p className={styles.container__header__button__text}>New Alias</p> */}
            </button>
          </>
        ) : (
          <>
            <h1 className={styles.container__header__phone__heading}>
              Your Identities
            </h1>
            <button
              className={styles.container__header__phone__button}
              onClick={() => {
                setNewAliasPopupOpen(true);
              }}
            >
              <div>
                <Add2 />
              </div>
              {/* <p className={styles.container__header__button__text}>New Alias</p> */}
            </button>
          </>
        )}
      </div>
      <div style={{ height: "10px" }}></div>
      {!loading && !master ? (
        <div className={styles.container__masterbanner}>
          <p className={styles.container__masterbanner__para}>
            You haven{"'"}t added a Master Password yet. Add one to add
            passwords to your identities that autofill into websites with the
            Browser Extension.
          </p>
          <Primary
            click={() => {
              setMasterPswdOpen(true);
            }}
            className={styles.container__masterbanner__button}
          >
            Add
          </Primary>
        </div>
      ) : null}
      <div className={styles.container__identities}>
        {loading ? (
          <div className="center" style={{ marginTop: "30vh" }}>
            <Loader />
          </div>
        ) : identities.length !== 0 ? (
          identities.map((identity, index) => {
            if (!isPhone) {
              return (
                <IdentityCard
                  reload={setHacky}
                  reloadValue={hacky}
                  identity={identity}
                  key={index}
                  isPhone={false}
                />
              );
            }

            return (
              <IdentityCard
                reload={setHacky}
                reloadValue={hacky}
                identity={identity}
                key={index}
                isPhone={true}
              />
            );
          })
        ) : (
          <Error>You don't have any identites</Error>
        )}
        {identities.length != 0 && !loading && (
          <div className={styles["thats-it"]}>That's It</div>
        )}
      </div>
      <div className={styles.container__list}></div>
      <Popup ref={aliasPopupRef} popupState={newAliasPopupOpen}>
        <NewIdentityPopup
          close={() => {
            setNewAliasPopupOpen(false);
          }}
          state={newAliasPopupOpen}
          reload={setHacky}
          reloadValue={hacky}
        />
      </Popup>

      <Popup ref={masterPswdPopupRef} popupState={masterPswdOpen}>
        <SetMaster
          close={() => {
            setMasterPswdOpen(false);
          }}
          state={masterPswdOpen}
          reload={setHacky}
          reloadValue={hacky}
          setMaster={setMaster}
        />
      </Popup>
    </div>
  );
}
