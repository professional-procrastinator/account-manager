import { useState, useEffect } from "react";
import styles from "../../../../styles/shared/popup.module.scss";
import Primary from "../../../../components/Button/Primary";
import RetryIcon from "../../../../public/icons/retry.svg";
import axios from "../../../../utils/axios";
import TextField from "../../../../components/TextField";
import showToast from "../../../../components/Toast";

export default function NewIdentityPopup({
  state,
  close,
  reload,
  reloadValue,
}) {
  const [identityEmail, setIdentityEmail] = useState("");
  const [websiteUrl, setWebsiteUrl] = useState("");
  const [error, setError] = useState(" ");
  const [creating, setCreating] = useState(false);

  const getNewIdentity = async () => {
    const { data } = await axios.get("/identities/generateId/");
    setIdentityEmail(data.id);
  };

  useEffect(() => {
    getNewIdentity();
  }, []);

  const newIdentity = async () => {
    setCreating(true);
    const { data } = await axios.post("/identities/", {
      email: identityEmail,
      websiteUrl,
    });
    if (!data.success) {
      setCreating(false);
      return setError(data.message);
    }

    setCreating(false);
    reload(!reloadValue);
    setIdentityEmail("");
    getNewIdentity();
    setError("");
    setWebsiteUrl("");
    close();
    showToast("Identity Created!");
  };

  useEffect(() => {
    if (!state) {
      setCreating(false);
      setError("");
      setWebsiteUrl("");
      getNewIdentity();
    }
  }, [state]);
  return (
    <form className={styles.main} onSubmit={(e) => {
      e.preventDefault();
      newIdentity();
    }}>
      <div className={styles.main__header}>
        <h2 className={styles.main__header__heading}>Create New Identity</h2>
      </div>

      <div className={styles.main__body}>
        <div className={styles.main__body__form} >
          <div className={styles.main__body__form__field}>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={identityEmail}
                disabled
                className={styles.main__body__form__field__content__input}
              />
              <div className={styles.main__body__form__field__content__action}>
                <RetryIcon onClick={getNewIdentity} />
              </div>
            </div>
            <label className={styles.main__body__form__field__label}>
              @altemail.co
            </label>
          </div>

          <div className={styles.main__body__form__field}>
            <label className={styles.main__body__form__field__label}>
              Website URL
            </label>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={websiteUrl}
                onChange={(e) => setWebsiteUrl(e.target.value)}
                className={styles.main__body__form__field__content__input}
              />
            </div>
          </div>
        </div>

        <div className={styles.main__body__error}>{error}</div>
      </div>
      <div className={styles.main__footer}>
        <Primary
          click={newIdentity}
          loading={creating}
          className={styles.main__footer__submit}
        >
          {creating ? "Creating..." : "Create"}
        </Primary>
      </div>
    </form>
  );
}
