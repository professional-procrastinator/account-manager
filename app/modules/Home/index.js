import { useState } from "react";
import styles from "./index.module.scss";

import Identities from "./Identities";
const tabs = ["Identities", "Accounts"];

const Home = () => {
  const [activeTab, setActiveTab] = useState("Identities");

  return (
    <>
      <div className={styles.main}>
        {/* {!phoneOrNot && <div height={{ height: "1200px" }}></div>} */}
        {/* <Tabs tabs={tabs} activeTab={activeTab} setActiveTab={setActiveTab} phone={phoneOrNot} /> */}
        {activeTab === "Identities" ? <Identities /> : null}
        {/* <div style={{ height: "20px" }}></div> */}
      </div>
    </>
  );
};

export default Home;
