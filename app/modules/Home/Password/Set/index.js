import { useState, useEffect, useContext } from "react";
import styles from "../../../../styles/shared/popup.module.scss";
import TextField from "../../../../components/TextField";
import Primary from "../../../../components/Button/Primary";
import axios from "../../../../utils/axios";
import showToast from "../../../../components/Toast";
export default function SetMaster({
  state,
  close,
  reload,
  reloadValue,
  setMaster,
}) {
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [setting, setSetting] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    if (!state) {
      setPassword("");
      setConfirmPassword("");
      setSetting(false);
      setError("");
    }
  }, [state]);
  const set = async () => {
    setSetting(true);
    if (password !== confirmPassword) {
      setSetting(false);
      return setError("Passwords do not match");
    }

    if (password == "" || password.trim() == "") {
      setSetting(false);
      return setError("Your password cannot be empty");
    }

    if (password.length < 6) {
      setSetting(false);
      return setError("Your password must be at least 6 characters");
    }

    const { data } = await axios.post("/auth/masterPassword", {
      masterPassword: password,
    });

    if (!data.success) {
      setSetting(false);

      return setError(data.message);
    }
    setMaster(true);

    setSetting(false);

    reload(true);
    showToast("Master Password Added!");
    return close();
  };
  return (
    <form
      className={styles.main}
      onSubmit={(e) => {
        console.log("hi");
        set();
      }}
    >
      <div className={styles.main__header}>
        <h2 className={styles.main__header__heading}>Set Master Password</h2>
      </div>

      <div className={styles.main__body}>
        <div className={styles.main__body__form}>
          <div className={styles.main__body__form__field}>
            <label className={styles.main__body__form__field__label}>
              Master Password
            </label>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={password}
                onChange={(e) => {
                  setError("");
                  setPassword(e.target.value);
                }}
                type="password"
                className={styles.main__body__form__field__content__input}
                password={true}
              />
            </div>
          </div>

          <div className={styles.main__body__form__field}>
            <label className={styles.main__body__form__field__label}>
              Confirm Master Password
            </label>
            <div className={styles.main__body__form__field__content}>
              <TextField
                value={confirmPassword}
                onChange={(e) => {
                  setError("");
                  setConfirmPassword(e.target.value);
                }}
                type="password"
                className={styles.main__body__form__field__content__input}
                password={true}
              />
            </div>
          </div>
        </div>

        <div className={styles.main__body__error}>{error}</div>
      </div>

      <div className={styles.main__footer}>
        <Primary
          click={(e) => {e.preventDefault(), set()}}
          loading={setting}
          className={styles.main__footer__submit}
        >
          {setting ? "Setting..." : "Set Password"}
        </Primary>
      </div>
    </form>
  );
}
