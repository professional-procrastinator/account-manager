import axios from "../../../utils/axios";
const login = async (email, password, setError) => {
  const { data } = await axios.post("/auth/login", {
    email: email,
    password: password,
  });

  if (data.success) {
    return data.user;
  }

  return setError(data.message);
};

const signup = async (name, email, password, setError) => {
  const  {data}  = await axios.post("/auth/register", {
    name: name,
    email: email,
    password: password,
  });
  if (data.success) {
    return data.data
  }
  return setError(data.message);
};

export { login, signup };
