import { useState, useEffect, useContext } from "react";
import styles from "../shared/form.module.scss";
import classnames from "classnames/bind";
import { login, signup as signupFN } from "../shared/form";
import { useRouter } from "next/router";
import useSession from "../../../utils/hooks/useSession";
import Primary from "../../../components/Button/Primary";
const cx = classnames.bind(styles);

import TextField from "../../../components/TextField";

export default function RegisterForm() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [registering, setRegistering] = useState(false);
  const { setUser } = useSession();
  const router = useRouter();
  const signUp = async (e) => {
    e.preventDefault();
    setRegistering(true);

    const user = await signupFN(name, email, password, setError);
    if (!user) {
      setRegistering(false);
      return setTimeout(() => setError(""), 7000);
    }
    setUser(user);
    router.push("/home");
  };

  return (
    <div className={cx("container")}>
      <div className={cx("container__header")}>
        <h1 className={cx("container__header__heading")}>Register</h1>
      </div>

      <form className={cx("container__content__form")} onSubmit={(e) => {
        login(e)
      }}>
          <div className={cx("container__content__form__field")}>
            <label className={cx("container__content__form__field__label")}>
              Name
            </label>

            <TextField
              className={cx("container__content__form__field__input")}
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>

          <div className={cx("container__content__form__field")}>
            <label className={cx("container__content__form__field__label")}>
              Email
            </label>

            <TextField
              className={cx("container__content__form__field__input")}
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>

          <div className={cx("container__content__form__field")}>
            <label className={cx("container__content__form__field__label")}>
              Password
            </label>

            <TextField
              className={cx("container__content__form__field__input")}
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              minLength={8}
              password={true}
            />
          </div>
          <div className={cx("container__content__form__error")}>
            <p>{error}</p>
          </div>
          <div className={cx("container__content__form__footer")}>
            <Primary
              className={cx("container__content__form__footer__submit", {
                container__content__form__footer__submit__loading: registering,
              })}
              loading={registering}
              click={signUp}
            >
              {registering ? "Registering..." : "Register"}
            </Primary>
          </div>
      </form>
    </div>
  );
}
