import { useState } from "react";
import styles from "../../../../styles/shared/popup.module.scss";
import TextField from "../../../../components/TextField";
import Primary from "../../../../components/Button/Primary";
import {
  ChangePass,
  ChangePassNotLogin,
} from "../../../../components/ChangePswd";

const MailForgot = () => {
  const [forgotEmail, setForgotEmail] = useState("");
  const [ifEmailPhase, setIfEmailPhase] = useState(true);
  if (ifEmailPhase) {
    return (
      <form
        className={styles.main}
        onSubmit={(e) => {
          e.preventDefault(), setIfEmailPhase(false);
        }}
      >
        <div className={styles.main__header}>
          <h2 className={styles.main__header__heading}>Enter Your Email ID </h2>
        </div>
        <div className={styles.main__container}>
          <span className={styles.main__container__text}>
            Enter The Email ID your registered your account with.
          </span>
          <br />
          <TextField
            className={styles.main__body__form__field__content__inputspecial}
            type="email"
            value={forgotEmail}
            onChange={(e) => setForgotEmail(e.target.value)}
            required
          />
        </div>

        <div className={styles.main__footer}>
          <Primary className={styles.main__footer__submit}>{"Next"}</Primary>
        </div>
      </form>
    );
  } else {
    return (
      <ChangePassNotLogin email={forgotEmail} forgotPhase={setIfEmailPhase} />
    );
  }
};
export default MailForgot;
