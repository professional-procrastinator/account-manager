import { useState, useEffect, useContext } from "react";
import styles from "../shared/form.module.scss";
import classnames from "classnames/bind";
const cx = classnames.bind(styles);
import useSession from "../../../utils/hooks/useSession";
import { login as loginFn } from "../shared/form";
import { useRouter } from "next/router";
import TextButton from "../../../components/Button/Text";
import TextField from "../../../components/TextField";
import Primary from "../../../components/Button/Primary";
import { Popup } from "../../../components/Popup";
import MailForgot from "./MailPopop";

export default function LoginForm({forgot}) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(" ");
  const [loggingIn, setLoggingIn] = useState(false);
  const router = useRouter();
  const { setUser } = useSession();

  const login = async (e) => {
    e.preventDefault();
    setLoggingIn(true);
    const user = await loginFn(email, password, setError);
    if (!user) {
      setLoggingIn(false);
      return setTimeout(() => setError(""), 7000);
    }
    setUser(user);
    router.push("/home");
  };
  return (
    <div className={cx("container")}>
      <div className={cx("container__header")}>
        <h1 className={cx("container__header__heading")}>Login</h1>
      </div>

      <form className={cx("container__content__form")} onSubmit={(e) => {
        
        login(e)
      }}>
        <div className={cx("container__content__form__field")}>
          <label className={cx("container__content__form__field__label")}>
            Email
          </label>

          <TextField
            className={cx("container__content__form__field__input")}
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>

        <div className={cx("container__content__form__field")}>
          <label className={cx("container__content__form__field__label")}>
            Password
          </label>

          <TextField
            className={cx("container__content__form__field__input")}
            type="password"
            value={password}
            password={true}
            onChange={(e) => setPassword(e.target.value)}
            required
            minLength={8}
          />
        </div>
        <div className={cx("container__content__form__error")}>
          <p>{error}</p>
        </div>
        <div className={cx("container__content__form__footer")}>
          <TextButton className={cx("container__content__form__footer__forgot")} click={() => forgot(true)} type="button">Forgot Password</TextButton>
          <Primary
            className={cx("container__content__form__footer__submit", {
              container__content__form__footer__submit__loading: loggingIn,
            })}
            loading={loggingIn}
            oncLick={(e) => {
              login(e)
            }}
          >
            {loggingIn ? "Logging in..." : "Login"}
          </Primary>

        </div>
      </form>
    </div>
  );
}
