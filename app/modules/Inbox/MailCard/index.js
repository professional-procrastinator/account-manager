import styles from './index.module.scss';
import { timeAgo } from '../../../utils/dates';
import Dot from '../../../public/icons/dot.svg';
import Dots from '../../../public/icons/dots.svg';
import { useMediaQuery } from 'react-responsive';
import classNames from 'classnames/bind';
import { useState, useRef } from 'react';
import Overlay from '../../../components/Overlay';
import Open from '../../../public/icons/open.svg';
import Delete from '../../../public/icons/delete.svg';
const cx = classNames.bind(styles);
export default function MailCard({ mail, click, onDelete }) {
  const isSeen = mail.flags.includes('\\Seen');
  const phone = useMediaQuery({ maxWidth: '651px' });
  const [isOverlayOpen, setOverlayOpen] = useState(false);
  const menuRef = useRef(null);

  console.log(phone);
  if (!phone) {
    return (
      <div
        onClick={() => {
          if (menuRef.current.contains(event.target)) {
            return;
          }
          click();
        }}
        className={cx({
          [styles.card]: true,
          [styles.card__seen]: isSeen,
          [styles.card__recent]: !isSeen,
        })}
      >
        <div className={styles.card__header}>
          <div className={styles.card__header__subject}>{mail.subject}</div>

          <div className={styles.card__header__to}>
            {mail.to.map((to, index) => {
              return (
                <div className={styles.card__header__to__text} key={index}>
                  {to.address}
                </div>
              );
            })}
          </div>

          <div
            ref={menuRef}
            style={{
              marginLeft: '12px',
            }}
          >
            <Overlay
              options={[
                {
                  name: 'Open',
                  icon: <Open />,
                  click: click,
                  danger: false,
                },

                {
                  name: 'Delete',
                  icon: <Delete />,
                  click: () => onDelete(mail.uid, mail.to[0].address),
                  danger: true,
                },
              ]}
            />
          </div>
        </div>

        <div className={styles.card__info}>
          <div className={styles.card__info__from}>
            {mail.from.map((from, index) => {
              return (
                <div className={styles.card__info__from__text} key={index}>
                  {from.name}
                </div>
              );
            })}
          </div>

          <Dot className={styles.card__info__separator} />

          <div className={styles.card__info__date}>{timeAgo(mail.date)}</div>
        </div>

        <div className={styles.card__body}>
          <div className={styles.card__body__text}>{mail.bodyText}</div>
        </div>
      </div>
    );
  } else {
    return (
      <div
        onClick={() => {
          if (menuRef.current.contains(event.target)) {
            return;
          }
          click();
        }}
        className={cx({
          [styles.card]: true,
          [styles.card__seen]: isSeen,
          [styles.card__recent]: !isSeen,
        })}
      >
        <div className={styles.phone__card__header}>
          <div className={styles.phone__card__header__from}>
            {mail.from.map((from, index) => {
              return (
                <div
                  className={styles.phone__card__header__from__text}
                  key={index}
                >
                  {from.name}
                </div>
              );
            })}
          </div>
          <div className={styles.phone__card__header__date}>
            {timeAgo(mail.date)}
          </div>
        </div>

        <div className={styles.phone__card__info}>
          <div className={styles.phone__card__info__to}>
            {mail.to.map((to, index) => {
              return (
                <div className={styles.phone__card__info__to__text} key={index}>
                  <span className={styles.phone__card__info__to__text__grey}>
                    to
                  </span>{' '}
                  {to.address}
                </div>
              );
            })}
          </div>
        </div>
        <div className={styles.phone__card__content}>
          <div className={styles.phone__card__content__subject}>
            {mail.subject}
          </div>
        </div>
      </div>
    );
  }
}
