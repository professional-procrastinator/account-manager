import axios from "../../../utils/axios";
import { useState, useEffect } from "react";
import Loader from "../../../components/Loader";
import MailCard from "../MailCard";
import DetailMail from "../DetailMail";
import styles from "./index.module.scss";
import Fuse from "fuse.js";

import Error from "../../../components/Error";
import showToast from "../../../components/Toast";

export default function Mail({
  identities,
  chosenIdentity,
  refresh,
  searchTerm,
  setHeaderVisible,
  mailLoading,
  inboxCleared,
  deletedMail,
  onDeleteMail,
}) {
  const [loading, setLoading] = useState(mailLoading);
  const [mails, setMails] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [mailMap, setMailMap] = useState({});
  const [prevRefresh, setPrevRefresh] = useState(refresh);
  const [prevInbox, setPrevInbox] = useState(false);
  const [prevDel, setPrevDel] = useState(false);
  const [isDetailMailOpen, setDetailMailOpen] = useState({
    isOpen: false,
    mail: null,
  });

  useEffect(() => {
    setLoading(mailLoading);
  }, [mailLoading]);

  useEffect(async () => {
    setPrevRefresh(refresh);
    if (!loading) {
      setLoading(true);
      setFetched(false);
      //   console.log(chosenIdentity);
      if (chosenIdentity == "All") {
        const result = await fetchAllMails(refresh != prevRefresh);
        setMails(result);
        if (inboxCleared != prevInbox) {
          showToast("Inbox Cleared!");
          setPrevInbox(!prevInbox);
        }
        if (deletedMail != prevDel) {
          showToast("Mail Deleted!");
          setPrevDel(!prevDel);
        }
      } else {
        const result = await fetchMail(chosenIdentity, refresh != prevRefresh);
        console.log("Result", result);
        setMails([...result]);
        // setPrevInbox(!prevInbox);
        if (inboxCleared != prevInbox) {
          showToast("Inbox Cleared!");
          setPrevInbox(!prevInbox);
        }
        if (deletedMail != prevDel) {
          showToast("Mail Deleted!");
          setPrevDel(!prevDel);
        }
      }
      setLoading(false);
      setFetched(true);
    }
  }, [chosenIdentity, refresh]);

  useEffect(async () => {
    if (fetched) {
      setLoading(true);
      const results = await filterSearch(searchTerm);
      setMails(results);
      setLoading(false);
    }
  }, [searchTerm, fetched]);

  const filterSearch = async (searchTerm) => {
    console.log("search term", searchTerm);

    if (searchTerm == " " || searchTerm == "") {
      return mailMap[
        chosenIdentity == "All" ? chosenIdentity : chosenIdentity.toLowerCase()
      ];
    } else {
      // filter here from mailMap[chosenIdentity] and return the results
      let results;

      const fuse = new Fuse(
        mailMap[
          chosenIdentity == "All"
            ? chosenIdentity
            : chosenIdentity.toLowerCase()
        ] || [],
        {
          keys: [
            "subject",
            "bodyText",
            "from.name",
            "from.address",
            "to.address",
          ],
        }
      );

      const fuseResults = fuse.search(searchTerm);
      results = fuseResults.map((result) => result.item);
      return results;
    }
  };

  const fetchAllMails = async (refreshBool) => {
    /*
        const { data } = await axios.get("/mail/all");
        if(data.success){
            setMails(data.mails);
        }
        setLoading(false);
        */
    console.log(refreshBool);
    if (mailMap["All"] && refreshBool == false) return mailMap["All"];

    const mailList = [];

    const result = await Promise.all(
      identities.map(async (id) => {
        const res = await axios.get(`/mail/${id.email}`);
        return res.data;
      })
    );
    // console.log("Identities Mail, ", identities);
    console.log(result);
    const returnMails = [];

    result.forEach((res) => {
      if (!res.success) return;
      const keys = Object.keys(res.mails);
      mailList.push(res.mailid.toLowerCase());

      keys.forEach((key) => {
        returnMails.push(res.mails[key]);
      });
    });

    const sortedMails = returnMails.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });

    const map = { All: sortedMails };

    mailList.forEach((m) => (map[m] = []));

    sortedMails.forEach((mail) => {
      let mailId;
      mail.to.forEach((to) => {
        // console.log(to.address.substring(to.address.length - 12));
        if (to.address.substring(to.address.length - 12) == "@altemail.co") {
          mailId = to.address.substring(0, mail.to[0].address.length - 12);
          //   console.log(mailId);
        }
      });
      // mailId = mailId.toLowerCase()
      console.log(mailId.toLowerCase());
      if (!map[mailId.toLowerCase()]) map[mailId.toLowerCase()] = [];
      map[mailId.toLowerCase()].push(mail);
    });

    console.log(mailList);

    setMailMap(map);
    return sortedMails;
  };

  const fetchMail = async (mail, refreshBool) => {
    // console.log(mail, mailMap[mailtoLowerCase().], refreshBool);
    if (mailMap[mail.toLowerCase()] && refreshBool == false) {
      console.log("here");
      return mailMap[mail.toLowerCase()];
    }

    console.log("here2");
    const allMails = [];

    const result = await axios.get(`/mail/${mail}`);

    if (result.data.success == false) return [];

    if (mailMap["All"]) {
      mailMap["All"].forEach((m) => {
        let mailId;
        m.to.forEach((to) => {
          // console.log(to.address.substring(to.address.length - 12));
          if (to.address.substring(to.address.length - 12) == "@altemail.co") {
            mailId = to.address.substring(0, m.to[0].address.length - 12);
            //   console.log(mailId);
          }
        });
        // console.log(mailId)
        if (mailId.toLowerCase() == mail.toLowerCase()) return;
        allMails.push(m);
      });
    }

    const rawMails = [];
    const keys = Object.keys(result.data.mails);
    keys.forEach((key) => {
      rawMails.push(result.data.mails[key]);
    });

    const sortedMails = rawMails.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });

    sortedMails.forEach((sm) => allMails.push(sm));

    const sortedAllMails = allMails.sort((a, b) => {
      return new Date(b.date) - new Date(a.date);
    });

    if (mailMap["All"]) {
      setMailMap((mm) => {
        return {
          ...mm,
          [mail.toLowerCase()]: sortedMails,
          All: sortedAllMails,
        };
      });
    } else {
      setMailMap((mm) => {
        return { ...mm, [mail.toLowerCase()]: sortedMails };
      });
    }
    return sortedMails;
  };

  if (loading) {
    return (
      <div className={styles["loader-container"]}>
        <Loader />
      </div>
    );
  }

  return (
    <>
      {!isDetailMailOpen.isOpen ? (
        <>
          {(!mails || mails.length == 0) && (
            <div>
              {/* {JSON.stringify(mails)} */}
              <Error>You don't have any mails</Error>
            </div>
          )}
          {mails &&
            mails.map((mail, i) => (
              <MailCard
                key={i}
                mail={mail}
                click={(e) => {
                  setHeaderVisible(false);
                  setDetailMailOpen({
                    isOpen: true,
                    mail: mail,
                  });
                }}
                onDelete={onDeleteMail}
              />
            ))}{" "}
          {mails && mails.length != 0 && (
            <div className={styles["thats-it"]}>That's It!</div>
          )}
        </>
      ) : (
        <DetailMail
          mail={isDetailMailOpen.mail}
          goBack={() => {
            setHeaderVisible(true);
            setDetailMailOpen({
              isOpen: false,
              mail: null,
            });
          }}
          onDelete={onDeleteMail}
        />
      )}
    </>
  );
}
