import styles from './index.module.scss';
import classNames from 'classnames/bind';
import useOnClickOutside from '../../../../components/Popup';
import { useEffect, useRef } from 'react';
import Back from '../../../../public/icons/back.svg';
import OpenIcon from '../../../../public/icons/open.svg';
import DownloadIcon from '../../../../public/icons/download.svg';
const cx = classNames.bind(styles);
export default function Preview({ attachment, open, close }) {
  const imageRef = useRef(null);
  const headerRef = useRef(null);
  const previewObjRef = useRef(null);

  useOnClickOutside(imageRef, (e) => {
    //ignore it if it is clicked inside header
    if (headerRef.current.contains(e.target)) {
      return;
    }
    try {
      if (document.pictureInPictureElement) {
        return close();
      }

      previewObjRef.current.pause();
    } catch (e) {}
    close();
  });

  return (
    <div
      className={cx(styles.overlay, {
        [styles['overlay--active']]: open,
      })}
    >
      <Header attachment={attachment} headerRef={headerRef} />
      <div
        ref={imageRef}
        className={cx(styles.preview, {
          [styles['preview--active']]: open,
        })}
      >
        {attachment.type === 'image' && (
          <img
            src={attachment.url}
            ref={previewObjRef}
            className={styles['preview__file']}
          />
        )}
        {attachment.type === 'video' && (
          <video
            src={attachment.url}
            ref={previewObjRef}
            controls
            className={styles['preview__file']}
          />
        )}

        {attachment.type === 'audio' && (
          <audio
            src={attachment.url}
            controls
            ref={previewObjRef}
            className={styles['preview__file']}
          />
        )}
      </div>
    </div>
  );
}

const Header = ({ attachment, headerRef }) => {
  const downloadlinkref = useRef(null);

  return (
    <div className={styles.overlay__header} ref={headerRef}>
      <div className={styles.overlay__header__title}>{attachment.name}</div>

      <div className={styles.overlay__header__actions}>
        <div className={styles.overlay__header__actions__download}>
          <DownloadIcon
            className={styles.overlay__header__actions__download__icon}
            onClick={(e) => {
              e.preventDefault();
              downloadlinkref.current.click();
            }}
          ></DownloadIcon>
          <a
            href={attachment.url}
            download={attachment.name}
            ref={downloadlinkref}
          />
        </div>
      </div>
    </div>
  );
};
