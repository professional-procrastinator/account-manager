import styles from "./index.module.scss";
import { useState, useEffect } from "react";
import Back from "../../../public/icons/back.svg";
import { timeAgo } from "../../../utils/dates";
import DownloadIcon from "../../../public/icons/download.svg";
import ImageIcon from "../../../public/icons/image.svg";
import VideoIcon from "../../../public/icons/video.svg";
import PDFIcon from "../../../public/icons/pdf.svg";
import FileIcon from "../../../public/icons/file.svg";
import niceBytes from "../../../utils/size";
import TextButton from "../../../components/Button/Text";
import { useRouter } from "next/router";
import Preview from "./Preview";
import { useMediaQuery } from "react-responsive";
export default function DetailMail({ mail, goBack, onDelete }) {
  const [attachments, setAttachments] = useState([]);
  const [bodyHtml, setBodyHtml] = useState("");
  const router = useRouter();
  const phone = useMediaQuery({ maxWidth: "651px" });
  useEffect(() => {
    mail.attachments.forEach((attachment) => {
      const arrayBuffer = new Uint8Array(attachment.content.data);

      //get data urls
      const blob = new Blob([arrayBuffer], { type: attachment.contentType });

      const reader = new FileReader();
      reader.readAsDataURL(blob);

      let url;
      reader.onloadend = () => {
        url = reader.result;

        setAttachments((prev) => [
          ...prev,
          {
            name: attachment.fileName,
            type:
              attachment.contentType === "application/pdf"
                ? attachment.contentType
                : attachment.contentType.split("/")[0],
            size: attachment.length,
            url: url,
          },
        ]);
      };
    });
    const newHTML = new DOMParser().parseFromString(mail.bodyHtml, "text/html");
    //change the background color to white for the body
    newHTML.body.style.backgroundColor = "white";

    setBodyHtml(newHTML.body.innerHTML);
  }, []);
  if (!phone) {
    return (
      <div className={styles.main}>
        <div className={styles.main__header}>
          <Back className={styles.main__header__back} onClick={goBack} />
          <TextButton
            className={styles.main__header__delete}
            click={() => {
              goBack();
              onDelete(mail.uid, mail.to[0].address);
            }}
          >
            Delete Email
          </TextButton>
        </div>

        <div className={styles.main__body}>
          <div className={styles.main__body__info}>
            <div className={styles.main__body__info__from}>
              {mail.from.map((from, index) => {
                return (
                  <div
                    className={styles.main__body__info__from__text}
                    key={index}
                  >
                    {console.log(from)}
                    {from.address}
                  </div>
                );
              })}
              <p>to</p>

              {mail.to.map((to, index) => {
                return (
                  <div
                    className={styles.main__body__info__from__text}
                    key={index}
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      window.location.href = `/inbox?mail=${to.address.replace(
                        "@altemail.co",
                        ""
                      )}`;
                    }}
                  >
                    {to.address}
                  </div>
                );
              })}
            </div>

            <div className={styles.main__body__info__date}>
              {timeAgo(mail.date)}
            </div>
          </div>

          <div className={styles.main__body__subject}>{mail.subject}</div>
          <div
            dangerouslySetInnerHTML={{
              __html: bodyHtml,
            }}
            className={styles.main__body__content}
          />

          {attachments.length > 0 && (
            <div className={styles.main__body__attachments}>
              <div className={styles.main__body__attachments__heading}>
                Attachments
              </div>

              <div className={styles.main__body__attachments__list}>
                {attachments.map((attachment, index) => {
                  return <Attachment key={index} attachment={attachment} />;
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  } else {
    return (
      <div className={styles.phone}>
        <div className={styles.phone__header}>
          <Back className={styles.phone__header__back} onClick={goBack} />
          <TextButton
            className={styles.phone__header__delete}
            click={() => {
              goBack();
              onDelete(mail.uid, mail.to[0].address);
            }}
          >
            DeletEmail
          </TextButton>
        </div>

        <div className={styles.phone__body}>
          <div className={styles.phone__body__info}>
            <div className={styles.phone__body__info__from}>
              {mail.from.map((from, index) => {
                return (
                  <div
                    className={styles.phone__body__info__from__text}
                    key={index}
                  >
                    <span
                      className={styles.phone__body__info__from__text__color}
                    >
                      From:{" "}
                    </span>{" "}
                    {from.address}
                  </div>
                );
              })}

              {mail.to.map((to, index) => {
                return (
                  <div
                    className={styles.phone__body__info__from__text}
                    key={index}
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      window.location.href = `/inbox?mail=${to.address
                        .replace("@altemail.co", "")
                        .toLowerCase()}`;
                    }}
                  >
                    <span
                      className={styles.phone__body__info__from__text__color}
                    >
                      To:{" "}
                    </span>
                    {to.address}
                  </div>
                );
              })}
            </div>
          </div>

          <div className={styles.phone__body__subject}>{mail.subject}</div>
          <div
            dangerouslySetInnerHTML={{
              __html: bodyHtml,
            }}
            className={styles.phone__body__content}
          />

          {attachments.length > 0 && (
            <div className={styles.phone__body__attachments}>
              <div className={styles.phone__body__attachments__heading}>
                Attachments
              </div>

              <div className={styles.phone__body__attachments__list}>
                {attachments.map((attachment, index) => {
                  return <Attachment key={index} attachment={attachment} />;
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

function Attachment({ attachment, index }) {
  const [preview, setPreview] = useState(false);
  const phone = useMediaQuery({ maxWidth: "651px" });
  if (!phone) {
    return (
      <>
        <Preview
          open={preview}
          close={() => {
            setPreview(false);
          }}
          attachment={attachment}
        />
        <div
          onClick={(e) => {
            setPreview(true);
          }}
          className={styles.main__body__attachments__list__item}
          key={index}
        >
          <div className={styles.main__body__attachments__list__item__top}>
            {attachment.type === "image" && (
              <ImageIcon
                style={{ height: "35px", width: "35px" }}
                className={styles.main__body__attachments__list__item__icon}
              />
            )}
            {attachment.type === "video" && (
              <VideoIcon
                style={{ height: "35px", width: "35px" }}
                className={styles.main__body__attachments__list__item__icon}
              />
            )}
            {attachment.type === "application/pdf" && (
              <FileIcon
                style={{ height: "35px", width: "35px" }}
                className={styles.main__body__attachments__list__item__icon}
              />
            )}
            {attachment.type !== "image" &&
              attachment.type !== "video" &&
              attachment.type !== "application/pdf" && (
                <FileIcon
                  style={{ height: "35px", width: "35px" }}
                  className={styles.main__body__attachments__list__item__icon}
                />
              )}
            <div className={styles.main__body__attachments__list__item__info}>
              <div
                className={
                  styles.main__body__attachments__list__item__info__name
                }
              >
                {attachment.name}
              </div>
              <div
                className={
                  styles.main__body__attachments__list__item__info__bottom
                }
              >
                <div
                  className={
                    styles.main__body__attachments__list__item__info__bottom__size
                  }
                >
                  {niceBytes(attachment.size)}
                </div>
              </div>
            </div>
          </div>
          <a
            className={
              styles.main__body__attachments__list__item__info__bottom__download
            }
            href={attachment.url}
            download={attachment.name}
          >
            <DownloadIcon
              className={
                styles.main__body__attachments__list__item__info__bottom__download__icon
              }
            />
          </a>
        </div>
      </>
    );
  } else {
    return (
      <>
        <Preview
          open={preview}
          close={() => {
            setPreview(false);
          }}
          attachment={attachment}
        />
        <div
          className={styles.phone__body__attachments__list__item}
          key={index}
        >
          <div
            onClick={(e) => {
              setPreview(true);
            }}
            className={styles.phone__body__attachments__list__item__top}
          >
            {attachment.type === "image" && (
              <ImageIcon
                style={{ height: "22px", width: "45px" }}
                className={styles.phone__body__attachments__list__item__icon}
              />
            )}
            {attachment.type === "video" && (
              <VideoIcon
                style={{ height: "22px", width: "45px" }}
                className={styles.phone__body__attachments__list__item__icon}
              />
            )}
            {attachment.type === "application/pdf" && (
              <FileIcon
                style={{ height: "22px", width: "45px" }}
                className={styles.phone__body__attachments__list__item__icon}
              />
            )}
            {attachment.type !== "image" &&
              attachment.type !== "video" &&
              attachment.type !== "application/pdf" && (
                <FileIcon
                  style={{ height: "22px", width: "45px" }}
                  className={styles.phone__body__attachments__list__item__icon}
                />
              )}
            <div className={styles.phone__body__attachments__list__item__info}>
              <div
                className={
                  styles.phone__body__attachments__list__item__info__name
                }
              >
                {attachment.name}
              </div>
              <div
                className={
                  styles.phone__body__attachments__list__item__info__bottom
                }
              >
                <div
                  className={
                    styles.phone__body__attachments__list__item__info__bottom__size
                  }
                >
                  {niceBytes(attachment.size)}
                </div>
              </div>
            </div>
          </div>
          <a
            className={
              styles.phone__body__attachments__list__item__info__bottom__download
            }
            href={attachment.url}
            download={attachment.name}
          >
            <DownloadIcon
              className={
                styles.phone__body__attachments__list__item__info__bottom__download__icon
              }
            />
          </a>
        </div>
      </>
    );
  }
}
