import Select from "react-select";
import styles from "./index.module.scss";
import { useMediaQuery } from "react-responsive";
import { useEffect, useState, useRef } from "react";
import { Popup, useOnClickOutside } from "../../../components/Popup";
import Filter from "../../../public/icons/filter.svg";
import Tick from "../../../public/icons/tick.svg";
export default function SelectInbox({
  identities,
  chosenIdentity,
  setChosenIdentity,
}) {
  // console.log("Identities Select, ", identities);
  const [selectIdentities, setSelectIdentities] = useState(identities);
  const phone = useMediaQuery({maxWidth: "651px"})
  const [selectOpen, setSelectOpen] = useState(false)
  const selectRef = useRef(null)
  useEffect(() => {
    const ids = [];
    ids.push({ email: "All" });
    identities.forEach((id) => {
      ids.push(id);
    });
    setSelectIdentities(ids);
  }, [identities]);

  console.log(chosenIdentity);

  useOnClickOutside(selectRef, () => setSelectOpen(false));
  const style = {
    control: (base, state) => ({
      ...base,
      paddingLeft: "-20px",
      fontWeight: "600",
      fontSize: "14px",
      marginLeft: "0",
      border: !state.isFocussed && "3px solid #e4e7eb",
      height: "56px",
      boxShadow: "none",
      "&:hover": {
        boxShadow: "none",
        cursor: "pointer",
      },
    }),
    menu: (base, state) => ({
      ...base,
      borderRadius: "0 0 6px 6px",
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      color: "brown",
      height: "30px",
    }),
    singleValue: (provided) => ({
      ...provided,
      color: "#3e4c59",
    }),
    option: (base, state) => ({
      ...base,
      paddingTop: "16px",
      paddingBottom: "16px",
      fontSize: "14px",
      fontWeight: "600",
      // borderRadius: '6px 6px 6px 6px',
      backgroundColor: state.isSelected ? "var(--primary-color-light)" : "#fff",
      color: state.isSelected ? "#F9F9F9" : "#3e4c59",
      "&:hover": {
        backgroundColor: state.isSelected
          ? "var(--primary-color-light)"
          : "var(--primary-button)",
        cursor: !state.isSelected ? "pointer" : "default",
      },
    }),
    menuList: (base, state) => ({
      ...base,
      paddingTop: "0",
      paddingBottom: "0",
    }),
  };
  if (!phone) {
  return (
    <Select
      value={chosenIdentity}
      // defaultValue={"All"}
      className={styles.select}
      options={selectIdentities}
      getOptionLabel={(option) => {
        return option.email == "All"
          ? option.email
          : `${option.email}@altemail.co`;
      }}
      styles={style}
      getOptionValue={(option) => {
        return option.email;
      }}
      theme={(theme) => ({
        ...theme,
        borderRadius: 6,
        colors: {
          ...theme.colors,
          text: "orangered",
          primary25: "rgba(#ef7b0f, 0.2)",
          primary: "#ef7b0f",
          neutral50: "#3e4c59",
          neutral80: "#3e4c59",
          neutral90: "#3e4c59",
        },
      })}
      placeholder={chosenIdentity === null ? "All" : chosenIdentity}
      menuPosition={"fixed"}
      onChange={(e) => {
        // console.log(e)
        setChosenIdentity(e.email);
      }}
    />
  );
    } else {
      return (
        <>
        
        <Filter className={styles.phone__filter} onClick={(e) => {setSelectOpen(!selectOpen), console.log(selectOpen)}}/>
        <Popup ref={selectRef } popupState={selectOpen} center={true}>
          <h1 className={styles.phone__heading}>Select Identity</h1>
          {selectIdentities.map((identity) => {
            return (
              <div className={styles.phone__container} onClick={(e) => {setChosenIdentity(identity.email), setSelectOpen(!selectOpen)}}>
                {console.log()}
                {identity.email === "All" ? <h2 className={styles.phone__container__text}>All IDs</h2> : <h2 className={styles.phone__container__text}>{identity.email}@altemail.co</h2>}
                {identity.email == chosenIdentity ? <Tick className={styles.phone__container__tick}/> : null}
              </div>
            )
          } )}
        </Popup>
        </>
    
      )
    }
}
