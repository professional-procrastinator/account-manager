import styles from "./index.module.scss";
import SearchIcon from "../../../public/icons/search.svg";
import { useMediaQuery } from "react-responsive";
export default function SearchBar({value, onChange}) {
  const phone = useMediaQuery({maxWidth: "651px"})
  if (!phone ) {
  return (
    <div className={styles.searchbar}>
      <input
        type="text"
        placeholder="Search..."
        className={styles.searchbar__input}
        value={value}
        onChange={onChange}
      />
      <SearchIcon className={styles.searchbar__icon} />
    </div>
  );
  } else {
    return (
      <div className={styles.phone__searchbar}>
      <input
        type="text"
        placeholder="Search..."
        className={styles.phone__searchbar__input}
        value={value}
        onChange={onChange}
      />
      <SearchIcon className={styles.phone__searchbar__icon} />
    </div>
    )
  }
}
