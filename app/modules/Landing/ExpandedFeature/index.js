import styles from "./index.module.scss";
import Image from "next/image";
import { useState } from "react";
const ExpandedFeature = (props) => {
    const [left, setLeft] = useState(props.left);
    console.log(props.left)

    return (
        <div>
            {left ? <div className={styles.container}>
                {console.log("hi")}
                <div className={styles.container__main}>
                    <h2 className={styles.container__main__heading}>{props.text}</h2>
                    <p className={styles.container__main__paragraph}>{props.para}</p>
                </div>
                <div className={styles.container__image}>
                    {console.log(props.image)}
                    <Image layout="fill" src={props.src} className={styles.container__image__im} />
                </div>
            </div>
                : <div className={styles.left}>
                    <div className={styles.left__image}>
                        {console.log(props.image)}
                        <Image layout={"fill"} src={props.src} className={styles.left__image__im} />
                    </div>
                    <div className={styles.left__main}>
                        <h2 className={styles.left__main__heading}>{props.text}</h2>
                        <p className={styles.left__main__paragraph}>{props.para}</p>
                    </div>
                </div>
            }

        </div>
    )
}
export default ExpandedFeature;