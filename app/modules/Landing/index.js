import styles from './index.module.scss';
import Secondary from '../../components/Button/Secondary';
import Image from 'next/image';
import DefaultLogo from '../../public/shield_big.svg';
import ExpandedFeature from './ExpandedFeature';
import FeatureCard from './FeatureCard/';
import PricingCard from './PricingCard';
import MainMockup from '../../public/main_mockup.svg';
const features = [
  {
    name: 'Manage aliases',
    text: 'Manage your aliases, protect them with passwords and keep your data safe. ',
  },
  {
    name: 'View your inbox',
    text: 'View your inbox, read your emails and download any attachments. ',
  },
  {
    name: 'Edit and Add Passwords',
    text: 'Add passwords to your identity, and keep them private using master passwords.',
  },
];

const pricing = [
  {
    heading: 'Basic',
    cool: false,
    features: [
      {
        there: true,
        name: 'I have a ',
      },
    ],
  },
  {
    heading: 'Standard',
    cool: true,
    features: [
      {
        there: true,
        name: 'very big thing ',
      },
    ],
  },
  {
    heading: 'Premium',
    cool: false,
    features: [
      {
        there: true,
        name: 'which you can lick',
      },
    ],
  },
];

const expanded_features = [
  {
    name: 'Manage aliases and protect your privacy',
    left: true,
    text: `Our app allows you to create new identities for websites you visit, not allowing them to form a connection of your data through cookies, keeping your 
    data safe and maintaining your privacy. You can delete them in a click and specify the website you want them for, making your accounts accesible and editable.`,
    image: '/mockup1.png',
  },
  {
    name: 'View Your inbox and manage your mails',
    left: false,
    text: `Unlike other apps and sites, the emails you create in our website are permanent and functional. Any E mails sent to them can be viewed through the emails tab
    along with their embedded html and attachments which can be downloaded.`,
    image: '/mockup2.png',
  },
  {
    name: 'Edit and add your passwords to protect your aliases',
    left: true,
    text: `You can add a master password and then edit your identites to add a password to each one of them, keeping them safe and private. Our built in password manager extension
    can allow you to add these passwords safely by using it's bleating edge encrypting techniques.`,
    image: '/mockup3.png',
  },
];
const Landing = () => {
  return (
    <div className={styles.container}>
      <div className={styles.container__main}>
        <div className={styles.container__main__content}>
          <div className={styles.container__main__content__text}>
            <h1 className={styles.container__main__content__text__heading}>
              Protect your privacy <br></br>with
            </h1>
            <h1 className={styles.container__main__content__text__name}>
              App Name
            </h1>
            <Secondary class={styles.container__main__content__text__button}>
              Get Started
            </Secondary>
          </div>
          <div className={styles.container__main__image}>
            <MainMockup className={styles.container__main__image__logo} />
          </div>
        </div>
      </div>
      <div className={styles.container__why}>
        <h2 className={styles.container__why__heading}>
          Why do you need{' '}
          <span className={styles.container__why__heading__color}>
            App Name
          </span>
          ?
        </h2>
        <p className={styles.container__why__content}>
          1-2 lines about the feature to be written here in piro english. 1-2
          lines about the feature to be written here in piro english. 1-2 lines
          about the feature to be written here in piro english. 1-2 lines about
          the feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english. 1-2 lines about the
          feature to be written here in piro english.
        </p>
      </div>

      <div className={styles.container__features}>
        {features.map((feature, index) => {
          return (
            <FeatureCard key={index} name={feature.name} text={feature.text} />
          );
        })}
      </div>
      <div className={styles.container__expanded}>
        {expanded_features.map((feature, index) => {
          return (
            <ExpandedFeature
              key={index}
              text={feature.name}
              para={feature.text}
              src={feature.image}
              left={feature.left}
            />
          );
        })}
      </div>
      <div className={styles.container__priceheading}>
        <h2 className={styles.container__priceheading__heading}>Pricing</h2>
      </div>
      <div className={styles.container__pricing}>
        {console.log(pricing)}
        {pricing.map((price, key) => {
          return (
            <PricingCard
              heading={price.heading}
              cool={price.cool}
              key={key}
              features={price.features}
            />
          );
        })}
      </div>
    </div>
  );
};

export default Landing;
