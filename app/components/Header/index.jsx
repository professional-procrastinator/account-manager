import styles from "./index.module.scss";
import Secondary from "../Button/Primary";
import Primary from "../Button/Secondary";
import classNames from "classnames/bind";
import Icon from "../../public/logo.svg";
const cx = classNames.bind(styles);
import { useRouter } from "next/router";
import useSession from "../../utils/hooks/useSession";
import { useMediaQuery } from "react-responsive";

function Header() {
  const router = useRouter();
  const { user } = useSession();
  const phone = useMediaQuery({ maxWidth: 676 });

  if (!user || !user.verified) {
    return (
      <div className={styles.header}>
        <div
          className={styles.header__logo}
          onClick={() => {
            router.push("/");
          }}
        >
          <Icon />
        </div>
        <div className={styles.header__links}>
          <Primary
            // text={"Login"}
            click={() => {
              router.push("/login");
            }}
            class={styles.header__links__action}
          >
            Login
          </Primary>
          <Secondary
            click={() => {
              router.push("/register");
            }}
            class={styles.header__links__action}
          >
            Register
          </Secondary>
        </div>
      </div>
    );
  }

  return !phone ? (
    <div className={styles.header}>
      <div
        className={styles.header__logo}
        onClick={() => {
          router.push("/");
        }}
      >
        <Icon />
      </div>

      <div className={styles.header__authLinks}>
        <a
          onClick={() => {
            router.push("/home");
          }}
          className={cx({
            [styles.header__authLinks__link]: true,
            [styles.header__authLinks__link__active]:
              router.pathname === "/home",
          })}
        >
          Home
        </a>
        <a
          onClick={() => {
            router.push("/inbox");
          }}
          className={cx({
            [styles.header__authLinks__link]: true,
            [styles.header__authLinks__link__active]:
              router.pathname === "/inbox",
          })}
        >
          Inbox
        </a>


        <a
          onClick={() => {
            router.push("/profile");
          }}
          className={cx({
            [styles.header__authLinks__link]: true,
            [styles.header__authLinks__link__active]:
              router.pathname === "/profile",
          })}
        >
          Profile
        </a>
      </div>
      <div
        className={styles.header__pfp}
        onClick={() => {
          router.push("/profile");
        }}
      >
        <img src={user.photoUrl} className={styles.header__pfp__image} />
      </div>
    </div>
  ) : (
    <div className={styles.phone__header}>
      <div
        className={styles.phone__header__logo}
        onClick={() => {
          router.push("/");
        }}
      >
        <Icon />
      </div>

      <div className={styles.phone__header__pfp}>
        <img src={user.photoUrl} className={styles.phone__header__pfp__image} />
      </div>
    </div>
  );
}
export default Header;
