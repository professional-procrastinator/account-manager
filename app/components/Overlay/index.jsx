import { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import Dots from '../../public/icons/dots.svg';
import Inbox from '../../public/icons/inbox.svg';
import classNames from 'classnames/bind';
import styles from './index.module.scss';
import axios from '../../utils/axios';
import showToast from '../Toast';

const cx = classNames.bind(styles);
const Overlay = ({ options, phone }) => {
  const [dotDiv, setDotDiv] = useState(false);
  const [hide, setHide] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const router = useRouter();

  let ref = useRef(null);

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      setHide(true);
      setTimeout(() => {
        setHide(false);
        setDotDiv(false);
      }, 400);
    }
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  });

  return (
    <>
      {dotDiv ? (
        <div
          className={cx({
            [styles.main__dotmenu]: true,
            [styles.hide]: hide,
          })}
          ref={ref}
        >
          {options.map((option, key) => {
            if (option.danger) {
              return (
                <div
                  key={key}
                  onClick={option.click}
                  className={styles.main__dotmenu__redcontainer}
                >
                  <div className={styles.main__dotmenu__redcontainer__image}>
                    <div
                      className={styles.main__dotmenu__redcontainer__image__img}
                    >
                      {option.icon}
                    </div>
                  </div>
                  <div
                    className={styles.main__dotmenu__redcontainer__headingred}
                  >
                    {option.name}
                  </div>
                </div>
              );
            }
            return (
              <div
                key={key}
                onClick={option.click}
                className={styles.main__dotmenu__container}
              >
                <div className={styles.main__dotmenu__container__image}>
                  {option.icon}
                </div>
                <div className={styles.main__dotmenu__container__heading}>
                  {option.name}
                </div>
              </div>
            );
          })}
        </div>
      ) : null}
      <Dots
        className={styles.main__phone__dots}
        onClick={(e) => {
          setDotDiv(!dotDiv);
        }}
      ></Dots>
    </>
  );
};
export default Overlay;
