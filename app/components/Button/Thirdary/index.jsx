import styles from "./index.module.scss";
import classNames from "classnames/bind";
import Loader from "../../Loader";
import Ripple from "../Ripple";
const cx = classNames.bind(styles);

const Thirdary = (props) => {
  console.log(props);
  return (
    <button
      style={props.style}
      className={cx({
        [props.className]: true,
        [styles.button]: !props.disabled && !props.loading,
        [styles.disabled]: props.disabled || props.loading,
        
      })}
      onClick={props.click}
    >
      <div className={styles.loader}>
        {props.loading ? (
          <Loader size={0.25} color={"var(--primary-color)"} />
        ) : null}
      </div>

      {props.text}
      {props.children}
      {!props.disabled && !props.loading && (
        <Ripple color="var(--secondary-ripple-color)" />
      )}
    </button>
  );
};
export default Thirdary;
