import styles from "./index.module.scss";
import classnames from "classnames/bind";
import Ripple from "../Ripple";
import Loader from "../../Loader";
const cx = classnames.bind(styles);

const DangerButton = ({ children, className, onClick, disabled, ...props }) => {
  return (
    <button
      className={cx(styles.button, className, {
        [styles['button--disabled']]: disabled || props.loading
      })}
      onClick={onClick}
      disabled={disabled}
      {...props}
    >
      <div className={styles.loader}>
        {props.loading ? <Loader size={0.25} color={"#ff0000"} /> : null}
      </div>
      {children}
      {!props.disabled && !props.loading && (
        <Ripple color="var(--danger-ripple-color)" />
      )}
    </button>
  );
};

export default DangerButton;
