import styles from "./index.module.scss";
import classNames from "classnames/bind";
import Router from "next/router";
import Ripple from "../Ripple";
const cx = classNames.bind(styles);

const TextButton = (props) => {
  return (
    <button
      style={props.style}
      className={cx({
        [styles.button]: true,
        [props.className]: true})}
      onClick={(e) => {props.click()}}
      type="button"
    >
      {props.text}
      {props.children}
      <Ripple/>
    </button>
  );
};
export default TextButton
