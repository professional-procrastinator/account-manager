import styles from "./index.module.scss";
import classNames from "classnames/bind";
import Router from "next/router";
import Ripple from "../Ripple";
import Loader from "../../Loader";
const cx = classNames.bind(styles);

const Primary = (props) => {
  return (
    <button
      className={cx(props.className, {
        [styles.primary]: !props.loading,
        [styles.primaryLoading]: props.loading,
      })}
      onClick={props.loading ? null : props.click}
    >
      <div className={styles.loader}>
        {props.loading ? <Loader size={0.25} color={"#fff"} /> : null}
      </div>
      {props.children}
      {!props.loading && <Ripple color="var(--primary-ripple-color)" />}
    </button>
  );
};

export default Primary;
