import styles from "./index.module.scss";
import classNames from "classnames/bind";
import Router from "next/router";
import Ripple from "../Ripple";
const cx = classNames.bind(styles);

const Secondary = (props) => {
  return (
    <button
      className={cx(styles.secondary, props.className)}
      onClick={props.click}
      style={props.style}
    >
      {props.text}
      {props.children}
      <Ripple/>
    </button>
  );
};
export default Secondary;
