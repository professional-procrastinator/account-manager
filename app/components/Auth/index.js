import useSession from "../../utils/hooks/useSession";
import { useRouter } from "next/router";

import Loader from "../Loader";
import { useEffect, useState } from "react";

const Auth = ({ children }) => {
  const { error, loading, user } = useSession();
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(true);

  //   consle.log(router.pathname);

  useEffect(() => {
    setIsLoading(true);
    console.log(user)
    if (!loading) {
      if (router.pathname === "/" || router.pathname.includes("/verify")) {
        if (router.pathname === "/") {
          if (user) {
            router.push("/home")
          }
          setIsLoading(false)
        }
        setIsLoading(false);
      } else if (!user && !["/login", "/register"].includes(router.pathname)) {
        router.push("/login");
        // setIsLoading(false);
      } else if (user) {
        setIsLoading(false)
        if (user.verified && ["/login", "/register"].includes(router.pathname)) {
        console.log("hi")
        router.push("/home");
        setIsLoading(false)
      } 
      else if (user.verified ==false) {
        console.log("hi")
        setIsLoading(false)
        
      } 
      }
      else {
        console.log("hi")
        setIsLoading(false);
      }
    }
  }, [loading, router.pathname]);

  if (isLoading) {
    return (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
          width: "100vw",
          // background: "red",
        }}
      >
        <Loader />
      </div>
    );
  } else {
    
    return <div>{children}</div>;
  }
};

export default Auth;
