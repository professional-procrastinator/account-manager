import { toast } from "react-toastify";

const showToast = (msg) => {
  return toast.success(msg, { autoClose: 4000, theme: "dark" });
};

const showErrorToast = (msg) => {
  return toast.error(msg, { autoClose: 4000, theme: "dark" });
};

export default showToast;
export { showErrorToast };
