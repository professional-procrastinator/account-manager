import styles from "./index.module.scss";
import classnames from "classnames/bind";
import OpenEyeIcon from "../../public/icons/eye_open.svg";
import ClosedEyeIcon from "../../public/icons/eye_closed.svg";
import { useState } from "react";
const cx = classnames.bind(styles);
export default function TextField({
  placeholder,
  className,
  disabled,
  value,
  onChange,
  type,
  password,
}) {
  const [passOpen, setPassOpen] = useState(false);
  if (!password) {
    return (
     
        <input
        className={cx("input", className)}
        placeholder={placeholder}
        disabled={disabled}
        value={value}
        onChange={onChange}
        type={type}
      /> 
      
    );
  }
  else {
    return (
    <div className={cx("container")}>
      <input
        className={cx("input", className)}
        placeholder={placeholder}
        disabled={disabled}
        value={value}
        onChange={onChange}
        type={passOpen ? "text" : "password"}
      />
      
      {!passOpen ? (<ClosedEyeIcon className={cx("eye")} onClick={(e) => {setPassOpen(!passOpen)}}/>) : 
      <OpenEyeIcon className={cx("eye")} onClick={(e) => {setPassOpen(!passOpen)}}/>}
    </div>
    )
  }
  
}
