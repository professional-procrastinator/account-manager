import React from 'react';
import Head from 'next/head';
import Header from '../Header';
import BottomHeader from '../BottomNav';
import { useMediaQuery } from 'react-responsive';
import styles from '../../styles/pages/main.module.scss';
import Auth from '../Auth';
import useSession from '../../utils/hooks/useSession';
import { useRouter } from 'next/router';
import Secondary from '../Button/Secondary';
import axios from '../../utils/axios';
import showToast, { showErrorToast } from '../Toast';
import dashStyles from './index.module.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

const Layout = ({ children, title }) => {
  const phone = useMediaQuery({ query: '(max-width: 676px)' });
  const { user } = useSession();
  const router = useRouter();
  const resendMail = async () => {
    const data = await axios.get(`/auth/resend/${user._id}`);
    if (data.data.success) {
      showToast(`Email sent to ${user.email}!`);
    } else {
      const error = data.data.message;
      showErrorToast(error);
    }
  };
  return (
    <>
      <Head>
        <title>Arcula</title>
      
        <meta name="title" content="Arcula" />
        <meta name="description" content="Treasure your privacy with Arcula." />
        <meta property="og:type" content="website" />
        <meta property="og:site_name" content="Arcula" />
        <meta property="og:url" content="https://arcula.co/" />
        <meta property="og:title" content="Arcula" />
        <meta
          property="og:description"
          content="Treasure your privacy with Arcula."
        />
        <meta
          property="og:image"
          content="https://media.discordapp.net/attachments/935516197175316503/947495900526305300/unknown.png"
        />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="628" />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://arcula.co/" />
        <meta property="twitter:title" content="Arcula" />
        <meta
          property="twitter:description"
          content="Treasure your privacy with Arcula."
        />
        <meta
          property="twitter:image"
          content="https://media.discordapp.net/attachments/935516197175316503/947495900526305300/unknown.png"
        />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge" />

        <title>{title || 'Arcula'}</title>
      </Head>
      <Auth>
        <div className={styles.main}>
          {router.pathname !== '/' ? <Header /> : null}
          {user ? (
            <div className={styles.main__content}>
              {user.verified ? (
                children
              ) : (
                <>
                  {router.pathname.includes('/login') ||
                  router.pathname.includes('/register') ||
                  router.pathname.includes('/verify') ? (
                    children
                  ) : (
                    <div className={dashStyles.verifyEmail}>
                      <h1 className={dashStyles.verifyEmail__heading}>
                        Verify Your account to access the dashboard
                      </h1>
                      <h2 className={dashStyles.verifyEmail__content}>
                        An Email has been sent to your account with instructions
                        on how to verify your account, verify your account and
                        then reload this page to access the dashboard.
                      </h2>
                      <div
                        style={{
                          marginLeft: 'auto',
                          marginRight: 'auto',
                          display: 'flex',
                          justifyContent: 'center',
                        }}
                      >
                        <Secondary
                          style={{ marginLeft: 'auto', marginRight: 'auto' }}
                          click={() => {
                            resendMail();
                          }}
                        >
                          Resend Email
                        </Secondary>
                      </div>
                    </div>
                  )}
                </>
              )}
            </div>
          ) : (
            <div
              className={cx({
                [styles.main__content]: true,
                [styles.main__landing]: router.pathname === '/',
              })}
            >
              {children}
            </div>
          )}
        </div>
        {phone ? <BottomHeader /> : null}
        <div id="popupContainer"></div>
      </Auth>
    </>
  );
};
export default Layout;
