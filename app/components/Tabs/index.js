import styles from "./index.module.scss";
import classnames from "classnames/bind";
const cx = classnames.bind(styles);
export default function Tabs({ tabs, activeTab, setActiveTab , phone}) {
  return (
    !phone ? (
    <div className={styles.tabs}>
      {tabs.map((tab, index) => {
        
        return (
          
          <div
            key={index}
            onClick={() => setActiveTab(tab)}
            className={cx(styles.tabs__tab, {
              [styles.tabs__tab__active]: tab === activeTab,
            })}
          >
            {tab}
          </div>
        );
      })}
    </div>
    ) : <div className={styles.phone__tabs}>
    {tabs.map((tab, index) => {
      
      return (
        
        <div
          key={index}
          onClick={() => setActiveTab(tab)}
          className={cx(styles.phone__tabs__tab, {
            [styles.phone__tabs__tab__active]: tab === activeTab,
          })}
        >
          {tab}
        </div>
      );
    })}
  </div>
  );
}
