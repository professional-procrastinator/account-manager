import styles from "./index.module.scss";

const Error = ({children, style}) => {
    return (
        <div className={styles.error} style={{style}}>
            <div className={styles.error__frowncontainer}>
                <h1 className={styles.error__frowncontainer__frown}> : (</h1>
            </div>
            <div className={styles.error__messagecontainer}>
                <h2 className={styles.error__messagecontainer__message}>{children}</h2>
            </div>
            
        </div>
    )
}
export default Error;