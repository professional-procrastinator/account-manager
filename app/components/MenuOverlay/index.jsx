import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Dots from "../../public/icons/dots.svg";
import Inbox from "../../public/icons/inbox.svg";
import Delete from "../../public/icons/delete.svg";
import classNames from "classnames/bind";
import styles from "./index.module.scss";
import axios from "../../utils/axios";
import showToast from "../Toast";

const cx = classNames.bind(styles);
const MenuOverlay = (props) => {
  const [dotDiv, setDotDiv] = useState(false);
  const [hide, setHide] = useState(false);
  const [deleting, setDeleting] = useState(false);
  const router = useRouter();

  const phone = props.phone;

  let ref = useRef(null);

  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      setHide(true);
      setTimeout(() => {
        setHide(false);
        setDotDiv(false);
      }, 400);
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
    return () => {
      document.removeEventListener("click", handleClickOutside, true);
    };
  });

  const deleteFN = () => {
    setDeleting(true);
    const res = axios
      .delete(`/identities/${props.identity._id}`)
      .then((res) => {
        // console.log("hi")
        console.log(props.reload, props.reloadValue);
        props.reload(!props.reloadValue);
        // console.log("by")
        setDeleting(false);
        showToast("Identity Deleted Successfully");
      });
  };
  return (
    <>
      {phone}
      {dotDiv ? (
        <div
          className={cx({
            [styles.main__dotmenu]: true,
            [styles.hide]: hide,
          })}
          ref={ref}
        >
          <div
            className={styles.main__dotmenu__container}
            onClick={() => {
              router.push(`/inbox?mail=${props.identity.email}`);
            }}
          >
            <div className={styles.main__dotmenu__container__image}>
              <Inbox className={styles.main__dotmenu__container__imageblue} />
            </div>

            <h2 className={styles.main__dotmenu__container__heading}>
              View Inbox
            </h2>
          </div>
          <div
            className={cx({
              [styles.main__dotmenu__redcontainer]: !deleting,
              [styles.main__dotmenu__redcontainer__deleting]: deleting,
              deleting,
            })}
            onClick={deleting ? null : deleteFN}
          >
            <div className={styles.main__dotmenu__redcontainer__image}>
              <Delete
                className={styles.main__dotmenu__redcontainer__image__img}
              />
            </div>
            <h2 className={styles.main__dotmenu__redcontainer__headingred}>
              {deleting ? "Deleting..." : "Delete Identity"}
            </h2>
          </div>
        </div>
      ) : null}
      {!phone ? (
        <Dots
          className={styles.main__dots}
          onClick={(e) => {
            setDotDiv(!dotDiv);
          }}
        />
      ) : (
        <Dots
          className={styles.main__phone__dots}
          onClick={(e) => {
            setDotDiv(!dotDiv);
          }}
        ></Dots>
      )}
    </>
  );
};
export default MenuOverlay;
