import styles from "./index.module.scss";
import Tick from "../../../public/icons/tick.svg";
const PricingCard = ({ cool, heading, features }) => {
  if (!cool) {
    return (
      <div className={styles.pricing__stdcard}>
        <h1 className={styles.pricing__stdcard__heading}>{heading}</h1>
        <div className={styles.pricing__stdcard__features}>
          {features.map((feature, i) => {
            return (
              <div
                key={i}
                className={styles.pricing__stdcard__features__feature}
              >
                <Tick
                  className={styles.pricing__stdcard__features__feature__icon}
                />
                <h2
                  className={styles.pricing__stdcard__features__feature__name}
                >
                  {feature.name}
                </h2>
              </div>
            );
          })}
        </div>
      </div>
    );
  } else {
    return (
      <div className={styles.pricing__highlightcard}>
        <h1 className={styles.pricing__highlightcard__heading}>{heading}</h1>
        <div className={styles.pricing__highlightcard__features}>
          {features.map((feature, i) => {
            return (
              <div
                key={i}
                className={styles.pricing__highlightcard__features__feature}
              >
                <Tick
                  className={
                    styles.pricing__highlightcard__features__feature__icon
                  }
                />
                <h2
                  className={
                    styles.pricing__highlightcard__features__feature__name
                  }
                >
                  {feature.name}
                </h2>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
};
export default PricingCard;
