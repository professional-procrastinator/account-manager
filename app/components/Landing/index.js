import styles from "./index.module.scss";
import Secondary from "../Button/Secondary";
import Image from "next/image";
import ExpandedFeature from "./ExpandedFeature";
import FeatureCard from "./FeatureCard/";
import PricingCard from "./PricingCard";
import Mockup1 from "../../public/mockup1.svg";
import Mockup2 from "../../public/mockup2.svg";
import WhiteLogo from "../../public/logo_white.svg";
import Mockup3 from "../../public/mockup3.svg";
import Footer from "./Footer";
import EndMockup from "../../public/endmockup.svg";
import Primary from "../Button/Primary";
import Thirdary from "../Button/Thirdary";
import WebIcon from "../../public/icons/web.svg";
import ChromeIcon from "../../public/icons/chrome.svg";
import { useRouter } from "next/router";

const features = [
  {
    name: "Manage aliases",
    text: "Manage your aliases, protect them with passwords and keep your data safe. ",
  },
  {
    name: "View your inbox",
    text: "View your inbox, read your emails and download any attachments. ",
  },
  {
    name: "Edit and Add Passwords",
    text: "Add passwords to your identity, and keep them private using master passwords.",
  },
];

const pricing = [
  {
    heading: "Basic",
    cool: false,
    features: [
      {
        there: true,
        name: "I have a ",
      },
    ],
  },
  {
    heading: "Standard",
    cool: true,
    features: [
      {
        there: true,
        name: "very big thing ",
      },
    ],
  },
  {
    heading: "Premium",
    cool: false,
    features: [
      {
        there: true,
        name: "which you can lick",
      },
    ],
  },
];

const expanded_features = [
  {
    name: "Manage aliases and protect your privacy",
    left: true,
    text: ["Never Gonna", "Give You Up"],
    image: Mockup1,
  },
  {
    name: "View Your inbox and manage your mails",
    left: false,
    text: ["Never Gonna", "Let You Down"],
    image: Mockup2,
  },
  {
    name: "Edit and add your passwords to protect your aliases",
    left: true,
    text: ["Never Gonna Run Around", "And Desert You"],
    image: Mockup3,
  },
];
const Landing = () => {
  const router = useRouter();

  return (
    <div className={styles.container}>
      <div className={styles.container__background}>
        <div className={styles.container__header}>
          <WhiteLogo className={styles.container__header__logo} />
          <Primary
            className={styles.container__header__primary}
            click={(e) => {
              router.push("/login");
            }}
          >
            Login
          </Primary>
          <Thirdary
            className={styles.container__header__thirdary}
            click={(e) => {
              router.push("/register");
            }}
          >
            Register
          </Thirdary>
        </div>
        <div className={styles.container__main}>
          <div className={styles.container__main__content}>
            <div className={styles.container__main__content__text}>
              <h1 className={styles.container__main__content__text__heading}>
                Treasure your privacy <br /> With
              </h1>
              <h2 className={styles.container__main__content__text__name}>
                Arcula
              </h2>

              <div className={styles.container__main__content__text__buttons}>
                <Thirdary
                  className={
                    styles.container__main__content__text__buttons__button
                  }
                  click={(e) => {
                    router.push("/register");
                  }}
                >
                  Get Started
                </Thirdary>
                <Thirdary
                  className={
                    styles.container__main__content__text__buttons__buttontwo
                  }
                >
                  Get Chrome Extension
                </Thirdary>
              </div>
              <div className={styles.container__main__content__text__mockups}>
                <Mockup1
                  className={
                    styles.container__main__content__text__mockups__mockup2
                  }
                />
                <Mockup2
                  className={
                    styles.container__main__content__text__mockups__mockup1
                  }
                />
                <Mockup3
                  className={
                    styles.container__main__content__text__mockups__mockup3
                  }
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.container__featurecontainer}>
        <div className={styles.container__features}>
          {features.map((feature, index) => {
            return (
              <FeatureCard
                key={index}
                name={feature.name}
                text={feature.text}
              />
            );
          })}
        </div>
      </div>

      <div className={styles.container__why}>
        <h2 className={styles.container__why__heading}>
          Take{" "}
          <span className={styles.container__why__heading__color}>
            Control{" "}
          </span>
          of your privacy.
        </h2>
        <p className={styles.container__why__content}>
          The internet today is{" "}
          <span className={styles.container__why__content__bold}>not</span> a
          private place. With big corporations and hacker groups alike trying to
          sell your data for their benefits you{" "}
          <span className={styles.container__why__content__bold}>need</span> to
          stay protected in this hostile place. This is where our app{" "}
          <span className={styles.container__why__content__app}>Arcula</span>{" "}
          comes in. Arcula aims towards a private and safe internet for anyone
          and everyone. With our ever expanding arsenal of user centric tools we
          at Arcula want to make your internet experience truly anonymous. We
          believe that with our bleating edge features like unlimited permanent
          fake email creation, cloud based secure user data encryption and
          features like accessible attachments in your mail we hope and believe
          that our app has a positive, productive, and{" "}
          <span className={styles.container__why__content__app}>private</span>{" "}
          impact on your life. We really hope you like our app it took lots of
          time, lots of work, lots of code and two all nighters to pull it off.
        </p>
      </div>
      <div className={styles.container__expandedcontainer}>
        <div className={styles.container__expanded}>
          {expanded_features.map((feature, index) => {
            return (
              <ExpandedFeature
                key={index}
                text={feature.name}
                para={feature.text}
                src={feature.image}
                left={feature.left}
              />
            );
          })}
        </div>
      </div>

      {/* <div className={styles.container__priceheading}>
        <h2 className={styles.container__priceheading__heading}>Pricing</h2>
      </div>
      <div className={styles.container__pricing}>
        {console.log(pricing)}
        {pricing.map((price, key) => {
          return (
            <PricingCard
              heading={price.heading}
              cool={price.cool}
              key={key}
              features={price.features}
            />
          );
        })}
      </div> */}
      <div className={styles.container__end}>
        <div className={styles.container__end__content}>
          <h2 className={styles.container__end__content__semi}>
            Don&apos;t risk your privacy anymore.
          </h2>
          <h1 className={styles.container__end__content__caption}>
            {" "}
            Start treasuring it with{" "}
            <span className={styles.container__end__content__caption__color}>
              Arcula
            </span>
            !
          </h1>
          <div className={styles.container__end__content__buttons}>
            <Primary
              className={styles.container__end__content__buttons__button}
              click={(e) => {
                router.push("/register");
              }}
            >
              <WebIcon />
              Get Started on the Web
            </Primary>
            <Thirdary
              className={styles.container__end__content__buttons__button}
            >
              <ChromeIcon />
              Download Chrome Extension
            </Thirdary>
          </div>
        </div>
        <EndMockup className={styles.container__end__content__mockup} />
      </div>
      <Footer />
    </div>
  );
};

export default Landing;
