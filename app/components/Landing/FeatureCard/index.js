import styles from "./index.module.scss";
import Image from "next/image";
import DefaultLogo from "../../../public/logo2.svg";
export default function FeatureCard({ name, text, logo }) {
  return (
    <div className={styles.card}>
      <div className={styles.card__header}>
        <DefaultLogo className={styles.card__header__logo}/>
      </div>

      <div className={styles.card__content}>
        <div className={styles.card__content__heading}>
          <h2 className={styles.card__content__heading__text}>{name}</h2>
        </div>
        <div className={styles.card__content__body}>
          <p className={styles.card__content__body__text}>{text}</p>
        </div>
      </div>
    </div>
  );
}
