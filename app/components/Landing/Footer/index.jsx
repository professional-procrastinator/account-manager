import TextIcon from "../../../public/textlogo.svg";
import styles from "./index.module.scss";

const Footer = () => {
    return (
        <div className={styles.container}>
            <div className={styles.container__content}>
                <TextIcon className={styles.container__content__logo}/>
                <h2 className={styles.container__content__copyright}>Copyright 2022 Arcula. All Rights Reserved</h2>
            </div>
            <div className={styles.container__links}>
                <h3>Privacy Policy</h3>
                <h3>Terms and Conditions</h3>
            </div>
        </div>
    )
}
export default Footer;