import styles from './index.module.scss';
import TickCircle from "../../../public/icons/tickcircle.svg"
import {useState} from 'react';
import Primary from '../../Button/Primary';
import { useRouter } from 'next/router';

const ExpandedFeature = props => {
  const router = useRouter();
  const [left, setLeft] = useState(props.left);
 
  return (
    <div>

      {left ? (
        <div className={styles.container}>
            <div className={styles.container__main}>
                <div className={styles.container__main__content}>
                    <h2 className={styles.container__main__content__heading}>{props.text}</h2>
                    <div className={styles.container__main__content__features}>
                  
                        {props.para.map((text, key) => {
                            return (
                                <div key={key} className={styles.container__main__content__features__item}>
                                    <TickCircle className={styles.container__main__content__features__item__tick}/>
                                    <h1 className={styles.container__main__content__features__item__text}>{text}</h1>
                                    </div>
                            )
                        })}
                    </div>
                    <Primary className={styles.container__main__content__button} click={() => {router.push("/register")}}> Get Started</Primary>
                </div>
                <div className={styles.container__main__image}>
                    <props.src className={styles.left__main__image__image}></props.src>
                </div>
            </div>
        </div>
      ) : (
        <div className={styles.left}>
            
            <div className={styles.left__main}>
            <div className={styles.left__main__image}>
                    <props.src className={styles.left__main__image__image}></props.src>
                </div>
                <div className={styles.left__main__content}>
                    <h2 className={styles.left__main__content__heading}>{props.text}</h2>
                    <div className={styles.left__main__content__features}>
                  
                        {props.para.map((text, key) => {
                            return (
                                <div key={key} className={styles.left__main__content__features__item}>
                                    <TickCircle className={styles.left__main__content__features__item__tick}/>
                                    <h1 className={styles.left__main__content__features__item__text}>{text}</h1>
                                    </div>
                            )
                        })}
                    </div>
                    <Primary className={styles.left__main__content__button} click={() => {router.push("/register")}}> Get Started</Primary>
                </div>
                
            </div>
        </div>
      )}
    </div>
  );
};
export default ExpandedFeature;
