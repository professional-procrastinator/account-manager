import { useState } from "react";
import styles from "../../styles/shared/popup.module.scss";
import TextButton from "../../components/Button/Text";
import Primary from "../Button/Primary";
import axios from "../../utils/axios";
import showToast, { showErrorToast } from "../Toast";
import useSession from "../../utils/hooks/useSession";
import BackIcon from "../../public/icons/back.svg";

const ChangePass = () => {
  const { user } = useSession();
  const [makingForgot, setMakingForgot] = useState(false);

  const forgotPass = async () => {
    setMakingForgot(true);
    const data = await axios.get(`/verify/send/${user.email}`);
    console.log(data);
    setMakingForgot(false);
    if (data.data.success) {
      showToast(`Email Sent`);
    } else {
      showErrorToast(data.data.message);
    }
  };
  const resendPass = async () => {
    setMakingForgot(true);
    const data = await axios.get(`/verify/resend/${user.email}`);
    console.log(data);
    setMakingForgot(false);
    if (data.data.success) {
      showToast(`Email Sent`);
    } else {
      showErrorToast(data.data.message);
    }
  };

  return (
    <div className={styles.main}>
      <div className={styles.main__header}>
        <h2 className={styles.main__header__heading}>Reset Your Password</h2>
      </div>
      <div className={styles.main__container}>
        <span className={styles.main__container__text}>
          An Email will be sent to <span>{user.email}</span> with further
          instructions to reset your password.
        </span>
      </div>

      <div className={styles.main__footer}>
        <TextButton className={styles.main__footer__resend} click={resendPass}>
          Resend Email
        </TextButton>
        <Primary
          className={styles.main__footer__submit}
          click={forgotPass}
          loading={makingForgot}
        >
          {!makingForgot ? "Send Email" : "Sending..."}
        </Primary>
      </div>
    </div>
  );
};

const ChangePassNotLogin = ({ email, forgotPhase }) => {
  const [makingForgot, setMakingForgot] = useState(false);

  const forgotPass = async () => {
    setMakingForgot(true);
    const data = await axios.get(`/verify/send/${email}`);
    console.log(data);
    setMakingForgot(false);
    if (data.data.success) {
      showToast(`Email Sent`);
    } else {
      const error = data.data.message;
      showErrorToast(error);
    }
  };
  const resendPass = async () => {
    setMakingForgot(true);
    const data = await axios.get(`/verify/resend/${email}`);
    console.log(data);
    setMakingForgot(false);
    if (data.data.success) {
      showToast(`Email Sent`);
    } else {
      const error = data.data.message;
      showErrorToast(error);
    }
  };

  return (
    <div className={styles.main}>
      <BackIcon
        style={{ tranform: "scaleX(0.1)" }}
        className={styles.cheese}
        onClick={(e) => forgotPhase(true)}
      />
      <div className={styles.main__header}>
        <h2 className={styles.main__header__heading}>Reset your Password</h2>
      </div>
      <div className={styles.main__container}>
        <span className={styles.main__container__text}>
          An Email will be sent to <span>{email}</span> with further
          instructions to reset your password.
        </span>
      </div>

      <div className={styles.main__footer}>
        <TextButton className={styles.main__footer__resend} click={resendPass}>
          Resend Email
        </TextButton>
        <Primary
          className={styles.main__footer__submit}
          click={forgotPass}
          loading={makingForgot}
        >
          {!makingForgot ? "Send Email" : "Sending..."}
        </Primary>
      </div>
    </div>
  );
};
export { ChangePassNotLogin, ChangePass };
