import styles from "./index.module.scss";
import Home from "../../public/icons/home.svg";
import Profile from "../../public/icons/profile.svg";
import Analytics from "../../public/icons/analytics.svg";
import Inbox from "../../public/icons/inbox.svg";
import {useRouter} from "next/router";
import classNames from "classnames/bind";
const cx = classNames.bind(styles);
import { useState } from "react";
import useSession from "../../utils/hooks/useSession";
const BottomHeader = () => {
    const router = useRouter();
    const [activeTab, setActiveTab] = useState(router.pathname);
    const {user} = useSession()
    if (user ) {
    return (
        <div className={styles.bottomHeader}>
            <div className={styles.bottomHeader__container}>
                <Home className={cx({
                    [styles.bottomHeader__icon]: true,
                    [styles.bottomHeader__orange]: activeTab === "/home"})} onClick={(e) => {
                        setActiveTab("/home");
                        router.push("/home");
                    }}/>
            </div>
            <div className={styles.bottomHeader__container}>
                <Inbox className={cx({
                    [styles.bottomHeader__icon]: true,
                    [styles.bottomHeader__orange]: activeTab === "/inbox"})} onClick={(e) => {
                        setActiveTab("/inbox");
                        router.push("/inbox");
                    }}/>
            </div>
            <div className={styles.bottomHeader__container}>
                <Analytics className={cx({
                    [styles.bottomHeader__icon]: true,
                    [styles.bottomHeader__orange]: activeTab === "/analytics"})} onClick={(e) => {
                        setActiveTab("/analytics");
                        router.push("/analytics");
                    }}/>
            </div>
            <div className={styles.bottomHeader__container}>
                <Profile className={cx({
                    [styles.bottomHeader__icon]: true, 
                    [styles.bottomHeader__orange]: activeTab === "/profile"})} onClick={(e) => {
                        setActiveTab("/profile");
                        router.push("/profile");
                    }}/>
            </div>
        </div>
    )
                } else {
                    return ( <div></div>)
                }
}
export default BottomHeader;