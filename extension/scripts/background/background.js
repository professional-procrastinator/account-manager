chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.type == 'hasIdentity') {
    chrome.storage.sync.get(['key'], (result) => {
      fetch(`https://arnav-001.el.r.appspot.com/identities/extension/${message.url}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          credentials: 'include',
          'X-Arcula-Extension-Security-Auth': result.key,
        },
      }).then((response) => {
        response.json().then((data) => {
          return sendResponse(data);
        });
      });
    });
  } else if (message.type == 'autofill') {
    const tab = sender.tab;

    chrome.tabs.sendMessage(tab.id, {
      type: 'autofill',
      identity: message.identity,
    });
  } else if (message.type == 'closeIFrame') {
    const tab = sender.tab;
    chrome.tabs.sendMessage(tab.id, {
      type: 'closeIFrame',
    });
  }
  return true;
});
