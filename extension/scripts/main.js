let user;
let container = document.getElementsByClassName('container')[0];
let loader = document.getElementsByClassName('loader-container')[0];

async function isLoggedIn() {
  container.style.display = 'none';

  chrome.storage.sync.get(['key'], function (result) {
    fetch(`${API_URL}/auth/extension/me`, {
      headers: {
        'X-Arcula-Extension-Security-Auth': result.key,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (!data.success) {
          return (window.location.href = 'login.html');
        }

        loader.style.display = 'none';
        container.style.display = 'flex';
      });
  });
}

isLoggedIn();

const settingsButton = document.getElementById('settingsButton');

settingsButton.addEventListener('click', () => {
  window.location.href = '/settings.html';
});
