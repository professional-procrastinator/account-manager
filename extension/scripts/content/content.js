const url = window.location.href;
const hostname = new URL(url).hostname || window.location.hostname;

let needsLogin;
let loginForm;
let settings;

const init = async () => {
  // ignore websites that are part of the google workspace
  if (shouldIgnore()) {
    return;
  }

  chrome.storage.sync.get(['settings'], function (result) {
    settings = result.settings;
  });

  // get the forms and inputs on the page
  const forms = document.getElementsByTagName('form');
  const inputs = document.getElementsByTagName('input');

  const { hasLoginForm, form } = formCheck(forms);

  if (hasLoginForm) {
    needsLogin = true;
    loginForm = form;
  }

  if (inputCheck(inputs)) {
    needsLogin = true;
  }

  const { hasIdentity, identity } = await hasIdentityAlready();

  if (hasIdentity) {
    if (settings.autofill) {
      return autofill(identity);
    }

    return launchIFrame(
      `/notifs/exists.html?url=${url}&identity=${JSON.stringify(identity)}`,
      'arcula-notification-iframe'
    );
  }

  if (!needsLogin) return;

  launchIFrame(`/notifs/create.html?url=${url}`, 'arcula-notification-iframe');
};

//helper functions
function formCheck(forms) {
  let result = {
    hasLoginForm: false,
    form: null,
  };
  Array.from(forms).forEach((form) => {
    //get all inputs in the form

    const formInputs = Array.from(form.getElementsByTagName('input'));

    //get all labels in the form
    const formLabels = Array.from(form.getElementsByTagName('label'));

    //get all spans in the form
    const formSpans = Array.from(form.getElementsByTagName('span'));

    formSpans.forEach((span) => {
      if (span.innerText.toLowerCase().includes('email')) {
        result = {
          hasLoginForm: true,
          form: form,
        };
      }
    });

    formLabels.forEach((label) => {
      if (label.textContent.toLowerCase().includes('email')) {
        result = {
          hasLoginForm: true,
          form: form,
        };
      }
    });

    formInputs.forEach((input) => {
      if (
        input.type === 'email' ||
        input.placeholder.toLowerCase().includes('email') ||
        input.name.toLowerCase().includes('email') ||
        input.autocomplete === 'email'
      ) {
        result = {
          hasLoginForm: true,
          form: form,
        };
      }
    });
  });

  return result;
}

function inputCheck(inputs) {
  let result = false;
  Array.from(inputs).forEach((input) => {
    if (
      input.placeholder.toLowerCase().includes('email') ||
      input.name.toLowerCase().includes('email') ||
      input.type === 'email' ||
      input.autocomplete === 'email'
    ) {
      result = true;
    }
  });

  return result;
}

function getAllSiblings(element, parent) {
  const children = [...parent.children];
  return children.filter((child) => child !== element);
}

function hasIdentityAlready() {
  const url = window.location.hostname;

  return new Promise((resolve, reject) => {
    let resp;
    chrome.runtime.sendMessage(
      {
        type: 'hasIdentity',
        url: url,
      },
      (res) => {
        if (res.identities.length > 0) {
          resolve({
            hasIdentity: true,
            identity: res.identities[0],
          });
        }

        resolve({
          hasIdentity: false,
          identity: null,
        });
      }
    );
  });
}

function launchIFrame(url, className) {
  const iframe = document.createElement('iframe');
  iframe.src = chrome.runtime.getURL(url);

  iframe.className = className;
  iframe.id = className;
  iframe.allowTransparency = true;
  iframe.allow = 'clipboard-write';

  const currentBGColor = document.body.style.backgroundColor;
  iframe.style.backgroundColor = currentBGColor;

  document.body.appendChild(iframe);
}

function shouldIgnore() {
  if (
    hostname.includes('google') ||
    hostname.includes('gmail') ||
    hostname.includes('forms') ||
    hostname.includes('localhost') ||
    hostname.includes('arcula')
  ) {
    return true;
  }

  return false;
}

function autofill(identity) {
  const loginFormInputs = Array.from(loginForm.getElementsByTagName('input'));

  loginFormInputs.forEach((input) => {
    if (
      input.placeholder.toLowerCase().includes('email') ||
      input.name.toLowerCase().includes('email') ||
      input.type === 'email' ||
      input.autocomplete === 'email'
    ) {
      input.value = `${identity.email}@altemail.co`;
      input.defaultValue = `${identity.email}@altemail.co`;
    }

    if (
      input.placeholder.toLowerCase().includes('password') ||
      input.name.toLowerCase().includes('password') ||
      input.type === 'password' ||
      input.autocomplete === 'password'
    ) {
      input.value = identity.websitePassword;
      input.defaultValue = identity.websitePassword;
    }
  });
}

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
  if (message.type === 'autofill') {
    autofill(message.identity);
  } else if (message.type === 'closeIFrame') {
    const iframe = document.getElementById('arcula-notification-iframe');
    iframe.classList.add('arcula-notification-iframe-hidden');
  }
});

init();
