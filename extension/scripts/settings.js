let userData;
const form = document.getElementById('settingsForm');

document.getElementById('logoutButton').addEventListener('click', logout);

const default_settings = {
  autofill: true,
};
async function isLoggedIn() {
  chrome.storage.sync.get('key', (data) => {
    fetch(`${API_URL}/auth/extension/me`, {
      headers: {
        'X-Arcula-Extension-Security-Auth': data.key,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.success) {
          userData = data.user;
          setUser();
          return data;
        }

        return (window.location.href = '/login.html');
      });
  });
}

await isLoggedIn();

function setUser() {
  const userImage = document.getElementById('userImage');
  const userName = document.getElementById('userName');
  const userEmail = document.getElementById('userEmail');

  userImage.src = userData.photoUrl;
  userName.innerText = userData.name;
  userEmail.innerText = userData.email;
}

function getSettings() {
  chrome.storage.sync.get('settings', (settings) => {
    if (!settings.settings) {
      chrome.storage.sync.set({ settings: default_settings });
    }

    const settingsForm = document.getElementById('settingsForm');

    settingsForm.autofill.checked = settings.settings.autofill;
  });
}

settingsForm.autofill.addEventListener('change', (event) => {
  const settings = {
    autofill: event.target.checked,
  };

  chrome.storage.sync.set({ settings });
});

getSettings();

function logout() {
  console.log('logout');
  chrome.storage.sync.get('key', (data) => {
    fetch(`${API_URL}/auth/extension/logout`, {
      method: 'POST',
      headers: {
        'X-Arcula-Extension-Security-Auth': data.key,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        return (window.location.href = '/login.html');
      });
  });
}
