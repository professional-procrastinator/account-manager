const identitiesDiv = document.getElementById('identities');

const getIdentities = async () => {
  const res = await fetch(`${API_URL}/identities`);
  const data = await res.json();
  console.log(data);
  return data.identities;
};

const renderIdentities = async () => {
  const identities = await getIdentities();
  if (identities.length == 0) {
    return;
  } else {
    identities.forEach((identity) => {
      const identityDiv = document.createElement('div');
      identityDiv.className = 'identity-div';
      const emailDiv = document.createElement('div');
      const emailText = document.createTextNode(
        identity.email + '@altemail.co'
      );
      const identityEmail = document.createElement('h2');
      identityEmail.appendChild(emailText);
      identityEmail.className = 'identity-email';
      emailDiv.appendChild(identityEmail);
      emailDiv.className = 'identity-middle';
      const identityImg = document.createElement('img');
      if (identity.websitePassword) {
        const lockTickImg = document.createElement('img');
        lockTickImg.src = '../icons/lock_tick.svg';

        lockTickImg.className = 'identity-locktick';
        emailDiv.appendChild(lockTickImg);
      }
      identityImg.src = identity.websitePhotoUrl;
      identityImg.className = 'identity-img';
      const copyDiv = document.createElement('div');
      copyDiv.className = 'identity-copy';
      const copyMailText = document.createTextNode('Copy Email');
      const copyMail = document.createElement('h2');

      copyMail.appendChild(copyMailText);
      copyMail.className = 'identity-copy-mail';
      copyMail.addEventListener('click', () => {
        navigator.clipboard.writeText(identity.email + '@altemail.co');
        copyMail.innerText = 'Copied!';
        setTimeout(function () {
          copyMail.innerText = 'Copy Email';
        }, 1000);
      });

      copyDiv.appendChild(copyMail);

      if (identity.websitePassword) {
        const copyPasswordText = document.createTextNode('Copy Password');
        const copyPassword = document.createElement('h2');
        copyPassword.className = 'identity-copy-password';
        copyPassword.appendChild(copyPasswordText);

        copyPassword.addEventListener('click', () => {
          navigator.clipboard.writeText(identity.websitePassword);
          copyPassword.innerText = 'Copied!';
          setTimeout(function () {
            copyPassword.innerText = 'Copy Email';
          }, 1000);
        });
        copyDiv.appendChild(copyPassword);
      }

      identityDiv.appendChild(identityImg);
      identityDiv.appendChild(emailDiv);
      identityDiv.appendChild(copyDiv);
      identitiesDiv.appendChild(identityDiv);
    });
  }
};
renderIdentities();
