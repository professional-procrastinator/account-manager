const registerLabel = document.getElementById('registerLabel');
const errorText = document.getElementById('errorText');

const emailInput = document.getElementById('emailInput');
const passwordInput = document.getElementById('passwordInput');
const loginButton = document.getElementById('loginButton');
const masterPasswordInput = document.getElementById('masterPasswordInput');

registerLabel.addEventListener('click', () => {
  chrome.tabs.create({ url: WEBSITE_URL });
});

const login = async (email, password, masterPassword) => {
  if (
    !email ||
    !password ||
    !masterPassword ||
    email.trim() === '' ||
    password.trim() === '' ||
    masterPassword.trim() === ''
  ) {
    return (
      (loginButton.disabled = false),
      loginButton.classList.remove('loading-button'),
      (loginButton.value = 'Login'),
      (errorText.innerText = 'All fields are required.')
    );
  }

  const response = await fetch(`${API_URL}/auth/extension/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email,
      password,
      masterPassword,
    }),
  });

  const data = await response.json();

  if (data.success) {
    chrome.storage.sync.set(
      {
        key: data.key,
      },
      () => {
        if (data.success) {
          return (window.location.href = 'popup.html');
        }

        return (
          (loginButton.disabled = false),
          loginButton.classList.remove('loading-button'),
          (loginButton.value = 'Login'),
          (errorText.innerText = data.message)
        );
      }
    );
  }
};

const submitForm = async (event) => {
  event.preventDefault();
  loginButton.disabled = true;
  loginButton.classList.add('loading-button');
  loginButton.value = 'Logging in...';

  const email = emailInput.value;
  const password = passwordInput.value;

  const masterPassword = masterPasswordInput.value;
  login(email, password, masterPassword);
};

document.getElementById('loginForm').addEventListener('submit', submitForm);
emailInput.addEventListener('keyup', () => {
  errorText.innerText = '';
});

passwordInput.addEventListener('keyup', () => {
  errorText.innerText = '';
});
