const websiteIcon = document.getElementById('websiteIcon');
const websiteURLText = document.getElementById('websiteURL');
const nameGeneratorTag = document.getElementById('nameGenerator');
const refreshImg = document.getElementById('refreshImg');
const createButton = document.getElementById('createButton');
let hostname;
let email;
const getParams = async () => {
  const params = new URLSearchParams(window.location.search);
  const url = params.get('url');

  hostname = new URL(url).origin;
  websiteURLText.innerText = hostname;

  const iconRes = await fetch(
    `https://besticon.herokuapp.com/allicons.json?url=${hostname}`
  );

  const iconData = await iconRes.json();

  let iconURL = '';

  iconData.icons.forEach((i) => {
    if (i.width > 0) {
      iconURL = i.url;
      websiteIcon.width = i.width;
    }
  });

  if (iconURL !== '') {
    websiteIcon.src = iconURL;
  } else {
    websiteIcon.src = '/icons/internet.png';
  }
};
const getName = async () => {
  const result = await fetch(`${API_URL}/identities/generateId`);
  const data = await result.json();

  email = data.id;
  nameGeneratorTag.value = email;
};
getName();
getParams();

const newIdentity = async () => {
  const resp = await fetch(`${API_URL}/identities/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      credentials: 'include',
    },
    body: JSON.stringify({
      websiteUrl: hostname,
      email: email,
    }),
  });
};

createButton.addEventListener('click', function () {
  newIdentity();
});
refreshImg.addEventListener('click', function () {
  getName();
});
