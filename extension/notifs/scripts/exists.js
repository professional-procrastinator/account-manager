const websiteIcon = document.getElementById('websiteIcon');
const websiteURLText = document.getElementById('websiteURL');
const identityName = document.getElementById('identityName');
const CopyMail = document.getElementById('CopyMail');
const CopyPass = document.getElementById('CopyPass');
const CopyInput = document.getElementById('CopyInput');
const AutoFillButton = document.getElementById('Autofill');

console.log(websiteIcon);
let userIdentity;
let password;

const getParams = async () => {
  const params = new URLSearchParams(window.location.search);
  const url = params.get('url');

  const identity = JSON.parse(params.get('identity'));
  identityName.innerText = identity.email + '@altemail.co';

  console.log(identity);
  const hostname = new URL(url).origin;
  userIdentity = identity;

  const iconRes = await fetch(
    `https://besticon.herokuapp.com/allicons.json?url=${hostname}`
  );
  const iconData = await iconRes.json();

  let iconURL = '';

  iconData.icons.forEach((i) => {
    if (i.width > 0) {
      iconURL = i.url;
      websiteIcon.width = i.width;
    }
  });

  if (iconURL !== '') {
    websiteIcon.src = iconURL;
  } else {
    websiteIcon.src = '/icons/internet.png';
  }

  websiteURLText.innerText = hostname;
  password = identity.websitePassword;

  if (identity.websitePassword) {
  } else {
    CopyPass.classList.add('password-disabled');
    CopyPass.disabled = true;
    CopyPass.innerText = 'No Password Added';
  }
};

CopyMail.addEventListener('click', () => {
  let textfield = document.createElement('textarea');
  textfield.innerText = userIdentity.email + '@altemail.co';

  document.body.appendChild(textfield);
  textfield.select();

  document.execCommand('copy');
  CopyMail.innerText = 'Copied!';

  setTimeout(function () {
    CopyMail.innerText = 'Copy Email';
  }, 1000);
});

CopyPass.addEventListener('click', () => {
  if (password) {
    let textfield = document.createElement('textarea');
    textfield.innerText = password;
    document.body.appendChild(textfield);
    textfield.select();
    document.execCommand('copy');
    CopyPass.innerText = 'Copied!';
    setTimeout(function () {
      CopyPass.innerText = 'Copy Password';
    }, 1000);
  }
});

AutoFillButton.addEventListener('click', () => {
  const params = new URLSearchParams(window.location.search);

  chrome.runtime.sendMessage('cgggmolijfbilimoinjejjkpipklblbl', {
    type: 'autofill',
    identity: userIdentity,
  });
});

getParams();
