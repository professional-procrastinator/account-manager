const closeButton = document.getElementsByClassName('notification-close')[0];

closeButton.addEventListener('click', () => {
  chrome.runtime.sendMessage({
    type: 'closeIFrame',
  });
});
